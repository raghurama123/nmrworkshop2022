export PATH=/home/Lib/openmpi-4.1.1-install/bin:$PATH
export LD_LIBRARY_PATH=/home/Lib/openmpi-4.1.1-install/lib:$LD_LIBRARY_PATH

export PATH=/home/Lib/ORCA_503:$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/Lib/ORCA_503

runorca () {
 rm -rf $mol
 mkdir $mol
 cd $mol
 echo $smi " " $mol > geom.smi
 obabel -oxyz geom.smi > geom.xyz --gen3d
 obminimize -oxyz -sd -ff uff geom.xyz > geom_uff.xyz
 mv geom_uff.xyz geom.xyz
 cp ../opt.com .
 /home/Lib/ORCA_503/orca opt.com > opt.out 
 cp opt.xyz geom.xyz
 cp ../nmr.com .
 /home/Lib/ORCA_503/orca nmr.com > nmr.out 
 cd ..
  }

mol="cyclohexane"
smi="C1CCCCC1"
runorca

mol="cyclohexene"
smi="C1=CCCCC1"
runorca

mol="cyclohexadiene"
smi="C1=CC=CCC1"
runorca

mol="benzene"
smi="c1ccccc1"
runorca
