import numpy as np

# Isotropic shielding of TMS
tms_H=31.7

# Isotropic shielding of cyclohexane
H=[ 30.012, 30.483, 30.009, 30.481, 30.484, 30.011, 30.012, 30.483, 30.481, 30.009, 30.011, 30.484 ]
delta = tms_H - np.array(H)
print(delta)

# Isotropic shielding of cyclohexene
H = [ 25.688,    25.685,    29.574,    29.668,    29.939,    30.175,    30.175,    29.935,    29.668,    29.573     ]
delta = tms_H - np.array(H)
print(delta)

# Isotropic shielding of cyclohexadiene
H = [ 25.613, 25.475, 25.476, 25.611, 29.615, 29.390, 29.394, 29.617  ]
delta = tms_H - np.array(H)
print(delta)

# Isotropic shielding of benzene
H=[ 24.049 , 24.048 , 24.050 , 24.049 , 24.048 , 24.051 ]
delta = tms_H - np.array(H)
print(delta)
