-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1784913345
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000189762 
   Number of Beta  Electrons                 16.0000189762 
   Total number of  Electrons                32.0000379525 
   Exchange energy                          -21.4715861669 
   Correlation energy                        -1.2750160165 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7466021835 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1784913345 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8299     6.0000     0.1701     4.2223     4.2223     0.0000
  1   0     8.3576     8.0000    -0.3576     2.1366     2.1366    -0.0000
  2   0     7.1913     7.0000    -0.1913     3.0474     3.0474    -0.0000
  3   0     6.1774     6.0000    -0.1774     3.8616     3.8616     0.0000
  4   0     0.9398     1.0000     0.0602     1.0025     1.0025     0.0000
  5   0     0.8495     1.0000     0.1505     1.0213     1.0213    -0.0000
  6   0     0.8878     1.0000     0.1122     0.9830     0.9830     0.0000
  7   0     0.8955     1.0000     0.1045     0.9906     0.9906     0.0000
  8   0     0.8712     1.0000     0.1288     1.0022     1.0022     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.006566
                0             6               2            7                1.200029
                0             6               4            1                0.984156
                2             7               3            6                0.894931
                2             7               5            1                0.973967
                3             6               6            1                0.975711
                3             6               7            1                0.978065
                3             6               8            1                0.968594
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.178491
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.9993897908
        Electronic Contribution:
                  0    
      0      -2.083447
      1       0.747088
      2      -2.430872
        Nuclear Contribution:
                  0    
      0       2.339336
      1      -2.020424
      2       3.319059
        Total Dipole moment:
                  0    
      0       0.255889
      1      -1.273337
      2       0.888187
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      23.396799  -0.443251   3.810608
      1       3.701929  23.577805   1.795678
      2       1.840525  -2.444146  22.732966
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.664775  -0.018793  -0.746808
      1       0.337027   0.884622  -0.322267
      2       0.666699  -0.465930  -0.581741
 P Eigenvalues: 
                  0          1          2    
      0      19.741881  23.656923  26.308767
P(iso)  23.235857
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      25.508664   2.266286   1.726001
      1       0.922379  30.092013  -2.923084
      2       2.960942  -1.660287  26.117791
 P Tensor eigenvectors:
                   0          1          2    
      0       0.676428   0.730566  -0.093377
      1      -0.337908   0.195185  -0.920718
      2      -0.654419   0.654352   0.378892
 P Eigenvalues: 
                  0          1          2    
      0      22.475742  28.053973  31.188753
P(iso)  27.239489
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.174993   0.735279  -0.109563
      1      -0.367832  28.215220   4.886916
      2      -0.484162   5.529755  32.170536
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.128169   0.991473  -0.023536
      1       0.811729   0.118508   0.571885
      2      -0.569798  -0.054193   0.819996
 P Eigenvalues: 
                  0          1          2    
      0      24.582784  27.212450  35.765516
P(iso)  29.186917
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      32.193260  -2.855123   4.024862
      1      -1.305456  28.535127  -0.361627
      2       3.389490  -0.307041  27.608860
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.526363  -0.143283   0.838100
      1      -0.244593  -0.918517  -0.310646
      2       0.814320  -0.368506   0.448427
 P Eigenvalues: 
                  0          1          2    
      0      25.353953  28.102032  34.881261
P(iso)  29.445749
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.641397   4.434931  -1.214424
      1       2.504981  27.209461  -2.040393
      2       0.012293  -0.951764  27.153627
 P Tensor eigenvectors:
                   0          1          2    
      0       0.505760   0.408234  -0.759969
      1      -0.795698  -0.119579  -0.593772
      2      -0.333274   0.905012   0.264353
 P Eigenvalues: 
                  0          1          2    
      0      24.441638  27.109922  32.452924
P(iso)  28.001495
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -1.895866 -37.894209 -73.189855
      1     -32.424492  41.938687  -8.485731
      2     -76.255461 -16.845783  24.146147
 P Tensor eigenvectors:
                   0          1          2    
      0       0.157974   0.715728   0.680278
      1      -0.844122   0.455345  -0.283052
      2       0.512349   0.529523  -0.676094
 P Eigenvalues: 
                  0          1          2    
      0      44.410876 -71.169045  90.947137
P(iso)  21.396323
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     171.763533   9.967820   8.328841
      1       9.132846 146.238898  16.534072
      2       8.537654  21.411948 154.655379
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.059760   0.677340  -0.733239
      1       0.785261  -0.421596  -0.453455
      2      -0.616274  -0.602883  -0.506694
 P Eigenvalues: 
                  0          1          2    
      0     130.887960 158.311295 183.458556
P(iso)  157.552603
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     165.253363 -42.517419  28.310975
      1     -70.694970  59.494758 -21.508727
      2      35.203109   8.677095 191.558781
 P Tensor eigenvectors:
                   0          1          2    
      0       0.356925  -0.648633   0.672220
      1       0.928184   0.327348  -0.176970
      2      -0.105261   0.687108   0.718889
 P Eigenvalues: 
                  0          1          2    
      0      35.792263 159.184948 221.329691
P(iso)  138.768967
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0      36.722299 -70.177791 -271.962626
      1     -120.524822 -297.656756 194.829487
      2     -245.007228 196.682267   4.748085
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.741523   0.657949  -0.131326
      1      -0.361346  -0.556566  -0.748106
      2      -0.565307  -0.507284   0.650454
 P Eigenvalues: 
                  0          1          2    
      0     -204.987159 277.834577 -329.033791
P(iso)  -85.395458
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2134
          Total Spin-Spin Coupling ISO:  31.0235 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3541
          Total Spin-Spin Coupling ISO:  10.4304 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4605
          Total Spin-Spin Coupling ISO:  -0.5479 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1045
          Total Spin-Spin Coupling ISO:  184.4412 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0300
          Total Spin-Spin Coupling ISO:  5.6622 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.9601
          Total Spin-Spin Coupling ISO:  0.2246 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.3085
          Total Spin-Spin Coupling ISO:  6.6352 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.6228
          Total Spin-Spin Coupling ISO:  2.8084 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2780
          Total Spin-Spin Coupling ISO:  0.6746 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 2.8389
          Total Spin-Spin Coupling ISO:  -0.7126 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0351
          Total Spin-Spin Coupling ISO:  -6.8921 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.1457
          Total Spin-Spin Coupling ISO:  0.3952 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.1898
          Total Spin-Spin Coupling ISO:  -0.1900 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.8329
          Total Spin-Spin Coupling ISO:  -0.2535 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.5818
          Total Spin-Spin Coupling ISO:  -0.1742 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4518
          Total Spin-Spin Coupling ISO:  7.9173 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0466
          Total Spin-Spin Coupling ISO:  14.2578 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0050
          Total Spin-Spin Coupling ISO:  64.6131 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1170
          Total Spin-Spin Coupling ISO:  -0.4107 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0838
          Total Spin-Spin Coupling ISO:  0.3029 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0785
          Total Spin-Spin Coupling ISO:  -0.6230 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.4007
          Total Spin-Spin Coupling ISO:  4.8435 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1333
          Total Spin-Spin Coupling ISO:  4.2935 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0924
          Total Spin-Spin Coupling ISO:  136.1979 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0882
          Total Spin-Spin Coupling ISO:  130.3565 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0877
          Total Spin-Spin Coupling ISO:  137.2655 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2495
          Total Spin-Spin Coupling ISO:  1.3102 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.8963
          Total Spin-Spin Coupling ISO:  -1.4693 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.1094
          Total Spin-Spin Coupling ISO:  -0.3404 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6740
          Total Spin-Spin Coupling ISO:  -1.3593 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.7213
          Total Spin-Spin Coupling ISO:  -0.2159 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.3583
          Total Spin-Spin Coupling ISO:  5.4670 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9392
          Total Spin-Spin Coupling ISO:  7.6407 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7760
          Total Spin-Spin Coupling ISO:  -10.0504 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7650
          Total Spin-Spin Coupling ISO:  -11.7163 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7750
          Total Spin-Spin Coupling ISO:  -12.1223 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.434033407882    0.272233951552   -0.415294888889
               1 O      1.589987766385    1.408199600549   -0.812152533164
               2 N      2.159663431061   -0.313287934996    0.566713725670
               3 C      3.213451618616    0.385471284294    1.280190712559
               4 H      0.672346774785   -0.408060661198   -0.835943918540
               5 H      1.913937783378   -1.248319679548    0.841142540105
               6 H      2.818291736363    0.987565551303    2.101509550142
               7 H      3.921874943245   -0.338148008988    1.678522715926
               8 H      3.727912538284    1.050665897031    0.590332096191
