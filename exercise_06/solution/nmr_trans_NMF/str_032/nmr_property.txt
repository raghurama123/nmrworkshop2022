-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1785425646
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999966757 
   Number of Beta  Electrons                 15.9999966757 
   Total number of  Electrons                31.9999933515 
   Exchange energy                          -21.4721263861 
   Correlation energy                        -1.2751402481 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7472666342 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1785425646 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8300     6.0000     0.1700     4.2226     4.2226     0.0000
  1   0     8.3574     8.0000    -0.3574     2.1378     2.1378     0.0000
  2   0     7.1908     7.0000    -0.1908     3.0468     3.0468    -0.0000
  3   0     6.1783     6.0000    -0.1783     3.8395     3.8395    -0.0000
  4   0     0.9404     1.0000     0.0596     1.0022     1.0022     0.0000
  5   0     0.8505     1.0000     0.1495     1.0206     1.0206    -0.0000
  6   0     0.8923     1.0000     0.1077     0.9895     0.9895    -0.0000
  7   0     0.8889     1.0000     0.1111     0.9836     0.9836    -0.0000
  8   0     0.8715     1.0000     0.1285     1.0139     1.0139     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.005963
                0             6               2            7                1.202140
                0             6               4            1                0.982836
                2             7               3            6                0.885314
                2             7               5            1                0.972981
                3             6               6            1                0.977717
                3             6               7            1                0.975925
                3             6               8            1                0.967822
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.178543
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.9797671539
        Electronic Contribution:
                  0    
      0      -1.713926
      1       0.484811
      2      -2.783605
        Nuclear Contribution:
                  0    
      0       1.625205
      1      -1.485098
      2       3.984881
        Total Dipole moment:
                  0    
      0      -0.088720
      1      -1.000287
      2       1.201276
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      23.836659   0.713901   4.178817
      1       3.769424  22.527974   1.018869
      2       0.212647  -2.575163  23.412315
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.605379  -0.182193   0.774804
      1       0.625661  -0.710656   0.321740
      2       0.492001   0.679539   0.544207
 P Eigenvalues: 
                  0          1          2    
      0      19.736490  23.768134  26.272325
P(iso)  23.258983
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.643645   3.590640   0.520698
      1       2.120582  27.638086  -3.879900
      2       1.640447  -2.191738  27.737503
 P Tensor eigenvectors:
                   0          1          2    
      0       0.590220  -0.737880   0.327372
      1      -0.635751  -0.174981   0.751799
      2      -0.497454  -0.651854  -0.572386
 P Eigenvalues: 
                  0          1          2    
      0      22.618498  28.223240  31.177496
P(iso)  27.339745
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.121892   1.338656   1.282183
      1       0.822223  25.860780   3.340058
      2       0.833867   1.659267  34.455664
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.256058  -0.950964   0.173498
      1       0.943938  -0.207291   0.256927
      2      -0.208363   0.229560   0.950730
 P Eigenvalues: 
                  0          1          2    
      0      24.989511  28.084637  35.364188
P(iso)  29.479445
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      35.269709  -2.977596   0.326321
      1      -2.305742  27.184563   1.013902
      2       0.999954   1.511085  25.624452
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.189155   0.216235  -0.957843
      1      -0.569091   0.770788   0.286392
      2       0.800222   0.599272  -0.022741
 P Eigenvalues: 
                  0          1          2    
      0      24.555458  27.459931  36.063336
P(iso)  29.359575
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.358359   2.852588   0.918217
      1       1.836447  30.334349  -1.696410
      2       2.284775   0.237410  26.083732
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.706431   0.574186  -0.413842
      1       0.333413  -0.245801  -0.910175
      2       0.624333   0.780956   0.017800
 P Eigenvalues: 
                  0          1          2    
      0      23.872497  27.485635  31.418309
P(iso)  27.592147
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -8.133104 -45.788824 -63.807184
      1     -38.856927  58.178168  -5.328276
      2     -70.613874 -10.430137  15.700393
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.103127   0.820649  -0.562051
      1      -0.628751   0.384066   0.676140
      2       0.770738   0.423118   0.476376
 P Eigenvalues: 
                  0          1          2    
      0      45.071911 -69.999815  90.673362
P(iso)  21.915152
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     176.055332   8.025069   7.520717
      1       4.585652 149.513319  20.123284
      2      10.932800  20.179009 151.366364
 P Tensor eigenvectors:
                   0          1          2    
      0       0.034353   0.614311  -0.788316
      1       0.714861  -0.566331  -0.410174
      2      -0.698422  -0.549446  -0.458602
 P Eigenvalues: 
                  0          1          2    
      0     130.193561 162.013967 184.727487
P(iso)  158.978338
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     126.535419 -52.474331  26.667975
      1     -70.368205  89.660729   4.630915
      2      56.915040  23.947191 197.025223
 P Tensor eigenvectors:
                   0          1          2    
      0       0.570432   0.621519   0.536956
      1       0.769080  -0.633666  -0.083567
      2      -0.288312  -0.460631   0.839461
 P Eigenvalues: 
                  0          1          2    
      0      36.609046 159.477884 217.134442
P(iso)  137.740457
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -4.663029 -219.240715 -150.320755
      1     -230.387155 -46.504025 228.788738
      2     -123.196565 290.992824 -185.296796
 P Tensor eigenvectors:
                   0          1          2    
      0       0.791738  -0.608401   0.054763
      1       0.361716   0.394691  -0.844619
      2       0.492252   0.688525   0.532560
 P Eigenvalues: 
                  0          1          2    
      0     -196.722873 266.985080 -306.726058
P(iso)  -78.821284
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2131
          Total Spin-Spin Coupling ISO:  30.9684 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3550
          Total Spin-Spin Coupling ISO:  10.3847 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4659
          Total Spin-Spin Coupling ISO:  -0.4973 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1044
          Total Spin-Spin Coupling ISO:  185.0837 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0283
          Total Spin-Spin Coupling ISO:  5.6563 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2726
          Total Spin-Spin Coupling ISO:  5.1838 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.0596
          Total Spin-Spin Coupling ISO:  0.6261 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5635
          Total Spin-Spin Coupling ISO:  3.9319 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2817
          Total Spin-Spin Coupling ISO:  0.7237 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 2.8539
          Total Spin-Spin Coupling ISO:  -0.7818 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0337
          Total Spin-Spin Coupling ISO:  -7.0067 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.1466
          Total Spin-Spin Coupling ISO:  0.5615 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.7702
          Total Spin-Spin Coupling ISO:  -0.1664 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.3758
          Total Spin-Spin Coupling ISO:  -0.1523 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4812
          Total Spin-Spin Coupling ISO:  0.1049 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4511
          Total Spin-Spin Coupling ISO:  7.9633 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0450
          Total Spin-Spin Coupling ISO:  14.3100 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0052
          Total Spin-Spin Coupling ISO:  64.4058 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0917
          Total Spin-Spin Coupling ISO:  0.2597 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1155
          Total Spin-Spin Coupling ISO:  -0.2130 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0680
          Total Spin-Spin Coupling ISO:  -0.7060 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.4022
          Total Spin-Spin Coupling ISO:  4.7733 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1315
          Total Spin-Spin Coupling ISO:  4.1532 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0892
          Total Spin-Spin Coupling ISO:  131.7552 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0924
          Total Spin-Spin Coupling ISO:  135.1016 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0869
          Total Spin-Spin Coupling ISO:  136.6880 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2426
          Total Spin-Spin Coupling ISO:  1.4574 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 4.0853
          Total Spin-Spin Coupling ISO:  -0.5338 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.9582
          Total Spin-Spin Coupling ISO:  -1.3030 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6343
          Total Spin-Spin Coupling ISO:  -1.1974 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.4143
          Total Spin-Spin Coupling ISO:  3.8848 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.6462
          Total Spin-Spin Coupling ISO:  -0.2801 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9592
          Total Spin-Spin Coupling ISO:  9.3987 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7751
          Total Spin-Spin Coupling ISO:  -9.6179 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7757
          Total Spin-Spin Coupling ISO:  -12.3138 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7677
          Total Spin-Spin Coupling ISO:  -11.9927 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.536671226358    0.198030384115   -0.510500444879
               1 O      1.972851934369    1.116429619048   -1.172229136808
               2 N      2.036532340164   -0.215817705142    0.679010542764
               3 C      3.190848406216    0.399636557763    1.307124354838
               4 H      0.661058878453   -0.397931263528   -0.823361359448
               5 H      1.598893652574   -1.005498376833    1.120960769731
               6 H      3.040428510368    0.462569173090    2.384064396282
               7 H      4.106962215745   -0.161797038154    1.109798946440
               8 H      3.307252835752    1.400698649642    0.900151931079
