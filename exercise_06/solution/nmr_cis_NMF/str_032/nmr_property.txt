-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1767054255
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000072002 
   Number of Beta  Electrons                 16.0000072002 
   Total number of  Electrons                32.0000144003 
   Exchange energy                          -21.4709582729 
   Correlation energy                        -1.2746790770 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7456373500 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1767054255 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8421     6.0000     0.1579     4.2607     4.2607    -0.0000
  1   0     8.3423     8.0000    -0.3423     2.1523     2.1523    -0.0000
  2   0     7.1708     7.0000    -0.1708     3.0332     3.0332    -0.0000
  3   0     6.1737     6.0000    -0.1737     3.8710     3.8710     0.0000
  4   0     0.9527     1.0000     0.0473     1.0030     1.0030    -0.0000
  5   0     0.8441     1.0000     0.1559     1.0299     1.0299    -0.0000
  6   0     0.8881     1.0000     0.1119     0.9832     0.9832     0.0000
  7   0     0.9007     1.0000     0.0993     1.0023     1.0023     0.0000
  8   0     0.8855     1.0000     0.1145     0.9715     0.9715     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.045838
                0             6               2            7                1.193835
                0             6               4            1                0.980933
                2             7               3            6                0.928874
                2             7               5            1                0.946995
                3             6               6            1                0.974891
                3             6               7            1                0.976672
                3             6               8            1                0.968724
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.176705
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.2386961516
        Electronic Contribution:
                  0    
      0      -3.391591
      1      -1.341333
      2      -2.682731
        Nuclear Contribution:
                  0    
      0       4.731677
      1       1.608455
      2       3.638605
        Total Dipole moment:
                  0    
      0       1.340086
      1       0.267122
      2       0.955874
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      24.123558   1.116536   2.436992
      1      -2.526449  23.865910   0.054157
      2       0.470122   2.355218  22.315027
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.444847  -0.186301  -0.876016
      1      -0.418690   0.907919   0.019528
      2       0.791714   0.375465  -0.481887
 P Eigenvalues: 
                  0          1          2    
      0      20.859660  24.512685  24.932148
P(iso)  23.434831
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.494715  -4.574336   0.029373
      1      -1.017245  31.245862   3.232321
      2       1.258880   0.676925  24.381410
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.461985   0.813847  -0.352454
      1      -0.359065   0.191744   0.913404
      2       0.810951   0.548533   0.203641
 P Eigenvalues: 
                  0          1          2    
      0      23.168973  26.147229  32.805785
P(iso)  27.373996
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.187343  -0.390254   4.084503
      1       0.060966  27.582311  -1.362628
      2       4.331594  -3.031046  31.250122
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.694361   0.488377   0.528536
      1       0.404760   0.872317  -0.274285
      2       0.595006  -0.023477   0.803379
 P Eigenvalues: 
                  0          1          2    
      0      24.729838  27.581841  34.708098
P(iso)  29.006592
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.502591   0.180790   0.566232
      1      -1.124974  29.098507   2.882672
      2      -0.566239   4.063676  28.880818
 P Tensor eigenvectors:
                   0          1          2    
      0       0.162508   0.983278  -0.082187
      1       0.686798  -0.052916   0.724920
      2      -0.708449   0.174251   0.683913
 P Eigenvalues: 
                  0          1          2    
      0      25.461899  27.533983  32.486033
P(iso)  28.493972
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      32.627024   4.346276  -1.404625
      1       4.722163  26.840474  -0.942949
      2      -0.593840   0.077396  27.339640
 P Tensor eigenvectors:
                   0          1          2    
      0       0.481286   0.108112  -0.869871
      1      -0.875630   0.105083  -0.471412
      2       0.040443   0.988569   0.145241
 P Eigenvalues: 
                  0          1          2    
      0      24.351718  27.195449  35.259971
P(iso)  28.935713
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      25.172555  60.991495   3.865458
      1      51.125644 -36.181226 -26.389547
      2       1.392497 -22.080405  66.943747
 P Tensor eigenvectors:
                   0          1          2    
      0       0.893675  -0.321467  -0.313057
      1       0.149705   0.871304  -0.467353
      2       0.423006   0.370795   0.826787
 P Eigenvalues: 
                  0          1          2    
      0      45.189925 -63.351977  74.097128
P(iso)  18.645025
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     159.040155  18.951707  17.368973
      1      19.823854 146.795946  10.338716
      2      14.139910   9.560640 160.747372
 P Tensor eigenvectors:
                   0          1          2    
      0       0.612583   0.433771   0.660746
      1      -0.787924   0.401329   0.467024
      2      -0.062595  -0.806708   0.587626
 P Eigenvalues: 
                  0          1          2    
      0     132.502837 147.460984 186.619653
P(iso)  155.527825
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     181.594283 -97.126170 -33.637110
      1     -57.295445 129.831632 -44.068551
      2      -8.521266 -72.942398 114.070898
 P Tensor eigenvectors:
                   0          1          2    
      0       0.507296  -0.469816  -0.722443
      1       0.618779  -0.384899   0.684810
      2       0.599803   0.794434  -0.095454
 P Eigenvalues: 
                  0          1          2    
      0      32.753093 154.900075 237.843645
P(iso)  141.832271
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -204.758483  55.235426 -260.368027
      1      92.393982 -117.622858 -163.864950
      2     -230.889837 -188.127331  72.770199
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.386018  -0.499045   0.775850
      1       0.882546  -0.444595   0.153130
      2       0.268520   0.743834   0.612052
 P Eigenvalues: 
                  0          1          2    
      0     -199.668119 297.537614 -347.480636
P(iso)  -83.203714
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2121
          Total Spin-Spin Coupling ISO:  31.0269 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3552
          Total Spin-Spin Coupling ISO:  10.8394 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4837
          Total Spin-Spin Coupling ISO:  4.1474 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1057
          Total Spin-Spin Coupling ISO:  182.2322 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0120
          Total Spin-Spin Coupling ISO:  -0.4704 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2720
          Total Spin-Spin Coupling ISO:  6.4374 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.6044
          Total Spin-Spin Coupling ISO:  6.7254 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.1112
          Total Spin-Spin Coupling ISO:  0.6506 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2797
          Total Spin-Spin Coupling ISO:  0.3077 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 3.6111
          Total Spin-Spin Coupling ISO:  0.6390 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0340
          Total Spin-Spin Coupling ISO:  -6.9285 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4955
          Total Spin-Spin Coupling ISO:  1.4899 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.3028
          Total Spin-Spin Coupling ISO:  -0.1576 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.8096
          Total Spin-Spin Coupling ISO:  0.5706 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.1899
          Total Spin-Spin Coupling ISO:  -0.3177 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4483
          Total Spin-Spin Coupling ISO:  8.6737 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0477
          Total Spin-Spin Coupling ISO:  13.9844 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0083
          Total Spin-Spin Coupling ISO:  63.1263 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0952
          Total Spin-Spin Coupling ISO:  0.1472 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0818
          Total Spin-Spin Coupling ISO:  -0.6382 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1178
          Total Spin-Spin Coupling ISO:  0.1905 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.6032
          Total Spin-Spin Coupling ISO:  1.8362 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1321
          Total Spin-Spin Coupling ISO:  5.2422 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0899
          Total Spin-Spin Coupling ISO:  133.3128 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0892
          Total Spin-Spin Coupling ISO:  130.0449 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0931
          Total Spin-Spin Coupling ISO:  135.5548 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9374
          Total Spin-Spin Coupling ISO:  9.2129 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.4971
          Total Spin-Spin Coupling ISO:  -0.6091 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2670
          Total Spin-Spin Coupling ISO:  0.1032 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.1936
          Total Spin-Spin Coupling ISO:  -0.5521 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.4215
          Total Spin-Spin Coupling ISO:  3.8586 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9731
          Total Spin-Spin Coupling ISO:  9.7419 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.6496
          Total Spin-Spin Coupling ISO:  -0.4767 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7652
          Total Spin-Spin Coupling ISO:  -12.9854 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7683
          Total Spin-Spin Coupling ISO:  -9.4956 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7687
          Total Spin-Spin Coupling ISO:  -11.0582 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.172372754811    0.250242895083   -0.095827570695
               1 O      0.389591477634   -0.305278992517   -0.836007742941
               2 N      2.352882625468   -0.267849009107    0.322117078022
               3 C      3.280584583514    0.404485495477    1.208093069234
               4 H      0.987819159203    1.258458332699    0.318942616611
               5 H      2.562295659841   -1.201188240935    0.003150830577
               6 H      3.661781681275   -0.288387614763    1.958070544926
               7 H      2.766922722187    1.215023079479    1.723359684647
               8 H      4.132649336065    0.826434054583    0.668761489619
