-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1755134256
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999949774 
   Number of Beta  Electrons                 15.9999949774 
   Total number of  Electrons                31.9999899549 
   Exchange energy                          -21.4704969043 
   Correlation energy                        -1.2744536862 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7449505905 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1755134256 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8413     6.0000     0.1587     4.2599     4.2599     0.0000
  1   0     8.3408     8.0000    -0.3408     2.1525     2.1525    -0.0000
  2   0     7.1782     7.0000    -0.1782     3.0129     3.0129    -0.0000
  3   0     6.1920     6.0000    -0.1920     3.8796     3.8796    -0.0000
  4   0     0.9485     1.0000     0.0515     1.0008     1.0008     0.0000
  5   0     0.8419     1.0000     0.1581     1.0296     1.0296     0.0000
  6   0     0.8823     1.0000     0.1177     0.9802     0.9802    -0.0000
  7   0     0.8806     1.0000     0.1194     0.9758     0.9758    -0.0000
  8   0     0.8944     1.0000     0.1056     0.9925     0.9925     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.047111
                0             6               2            7                1.188379
                0             6               4            1                0.983394
                2             7               3            6                0.917473
                2             7               5            1                0.949241
                3             6               6            1                0.975316
                3             6               7            1                0.973085
                3             6               8            1                0.977680
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.175513
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.2552962302
        Electronic Contribution:
                  0    
      0      -3.364792
      1      -1.275976
      2      -2.768427
        Nuclear Contribution:
                  0    
      0       4.760059
      1       1.601904
      2       3.634284
        Total Dipole moment:
                  0    
      0       1.395267
      1       0.325928
      2       0.865857
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      24.848707   0.689922   2.149977
      1      -1.744055  21.835829   1.264950
      2      -1.404928   1.986022  23.068253
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.151243  -0.052859   0.987082
      1      -0.812980   0.574688  -0.093791
      2       0.562307   0.816664   0.129891
 P Eigenvalues: 
                  0          1          2    
      0      20.615286  24.191827  24.945675
P(iso)  23.250929
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.246348  -3.999899  -4.139678
      1      -1.170666  26.177942   4.229582
      2      -0.288031   3.304924  27.648706
 P Tensor eigenvectors:
                   0          1          2    
      0       0.153525   0.828754  -0.538141
      1       0.803873   0.211956   0.555754
      2      -0.574646   0.517920   0.633673
 P Eigenvalues: 
                  0          1          2    
      0      22.984358  26.011465  33.077172
P(iso)  27.357665
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.091438   1.220351   1.602780
      1       1.192729  25.775270  -0.928405
      2      -0.345561  -0.961700  34.024135
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.649687   0.758813   0.045932
      1       0.752524   0.650515  -0.102653
      2       0.107774   0.032127   0.993656
 P Eigenvalues: 
                  0          1          2    
      0      24.583447  27.148124  34.159272
P(iso)  28.630281
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.279276   3.481432   1.921469
      1       1.881945  33.273716   0.820637
      2       1.561379   1.553063  26.565048
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.804999  -0.493948   0.328623
      1       0.158875   0.354200   0.921575
      2       0.571608  -0.794077   0.206655
 P Eigenvalues: 
                  0          1          2    
      0      24.476206  27.097076  34.544757
P(iso)  28.706013
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      33.561907  -1.289255   1.370124
      1      -0.368164  26.546343   1.146955
      2       2.725224   0.717248  27.374999
 P Tensor eigenvectors:
                   0          1          2    
      0       0.234007   0.149626  -0.960652
      1       0.744109  -0.663499   0.077916
      2      -0.625734  -0.733063  -0.266601
 P Eigenvalues: 
                  0          1          2    
      0      25.498698  27.770778  34.213772
P(iso)  29.161083
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -6.775365  47.983641  45.090517
      1      40.372769  38.400885 -51.619306
      2      35.104928 -49.747134  21.234044
 P Tensor eigenvectors:
                   0          1          2    
      0       0.854622  -0.502133  -0.132227
      1       0.197273   0.549539  -0.811843
      2       0.480317   0.667735   0.568706
 P Eigenvalues: 
                  0          1          2    
      0      39.655545 -69.109980  82.314000
P(iso)  17.619855
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     153.396683  13.112870  20.968934
      1      14.648193 150.526202   8.779696
      2      21.534457   8.100091 164.375112
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.786004  -0.146073   0.600717
      1       0.444920  -0.808305   0.385601
      2       0.429237   0.570355   0.700322
 P Eigenvalues: 
                  0          1          2    
      0     134.026651 147.047318 187.224028
P(iso)  156.099332
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     226.982289 -56.203475 -60.842472
      1     -31.538788 133.593038 -37.165833
      2     -22.626599 -44.997835  83.474576
 P Tensor eigenvectors:
                   0          1          2    
      0       0.375553   0.157935  -0.913245
      1       0.482569   0.807941   0.338170
      2       0.791257  -0.567705   0.227210
 P Eigenvalues: 
                  0          1          2    
      0      43.307974 153.743801 246.998128
P(iso)  148.016634
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -322.995571  20.622173 -143.740611
      1      44.848808 126.216053 -252.253637
      2     -104.574874 -259.358225 -91.934675
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.566296   0.158885  -0.808742
      1       0.534237   0.817959  -0.213387
      2       0.627614  -0.552900  -0.548089
 P Eigenvalues: 
                  0          1          2    
      0     -206.864607 309.886494 -391.736080
P(iso)  -96.238064
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2126
          Total Spin-Spin Coupling ISO:  31.2406 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3542
          Total Spin-Spin Coupling ISO:  10.7043 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4812
          Total Spin-Spin Coupling ISO:  3.7058 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1061
          Total Spin-Spin Coupling ISO:  180.6209 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0135
          Total Spin-Spin Coupling ISO:  -0.8533 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.8045
          Total Spin-Spin Coupling ISO:  1.8888 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.8684
          Total Spin-Spin Coupling ISO:  0.7369 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.3438
          Total Spin-Spin Coupling ISO:  11.3800 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2790
          Total Spin-Spin Coupling ISO:  0.3519 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 3.6109
          Total Spin-Spin Coupling ISO:  0.4916 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0336
          Total Spin-Spin Coupling ISO:  -6.8975 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4986
          Total Spin-Spin Coupling ISO:  1.4792 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.9627
          Total Spin-Spin Coupling ISO:  0.0436 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.0100
          Total Spin-Spin Coupling ISO:  -0.0839 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.3531
          Total Spin-Spin Coupling ISO:  0.0555 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4527
          Total Spin-Spin Coupling ISO:  8.5575 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0488
          Total Spin-Spin Coupling ISO:  13.9269 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0074
          Total Spin-Spin Coupling ISO:  64.1048 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1129
          Total Spin-Spin Coupling ISO:  -0.2703 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1194
          Total Spin-Spin Coupling ISO:  -0.1644 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0749
          Total Spin-Spin Coupling ISO:  0.0637 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5980
          Total Spin-Spin Coupling ISO:  1.5769 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1377
          Total Spin-Spin Coupling ISO:  5.3418 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0912
          Total Spin-Spin Coupling ISO:  134.2420 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0919
          Total Spin-Spin Coupling ISO:  135.0470 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0873
          Total Spin-Spin Coupling ISO:  129.9385 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9396
          Total Spin-Spin Coupling ISO:  10.0423 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.6322
          Total Spin-Spin Coupling ISO:  -0.1302 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.7498
          Total Spin-Spin Coupling ISO:  -0.1865 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6391
          Total Spin-Spin Coupling ISO:  -0.5767 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.8812
          Total Spin-Spin Coupling ISO:  3.5586 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.8443
          Total Spin-Spin Coupling ISO:  1.9263 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.3050
          Total Spin-Spin Coupling ISO:  7.4563 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7749
          Total Spin-Spin Coupling ISO:  -8.2942 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7596
          Total Spin-Spin Coupling ISO:  -13.1638 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7621
          Total Spin-Spin Coupling ISO:  -12.6517 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.094452601235    0.096956288672    0.069747590131
               1 O      0.366017476410   -0.357658365818   -0.786423220380
               2 N      2.430856672620   -0.108631265376    0.145459873929
               3 C      3.285811720617    0.449813863540    1.178640820026
               4 H      0.720074070791    0.737979301225    0.889750171346
               5 H      2.833064260326   -0.690245377413   -0.572031066336
               6 H      2.944687352691    0.168052705635    2.176135014374
               7 H      3.340580433322    1.538791439909    1.120304261963
               8 H      4.291355411989    0.056881409626    1.049076554948
