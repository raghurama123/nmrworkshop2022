-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1756048922
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000287836 
   Number of Beta  Electrons                 16.0000287836 
   Total number of  Electrons                32.0000575671 
   Exchange energy                          -21.4705248803 
   Correlation energy                        -1.2744699989 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7449948792 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1756048922 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8408     6.0000     0.1592     4.2597     4.2597     0.0000
  1   0     8.3410     8.0000    -0.3410     2.1523     2.1523     0.0000
  2   0     7.1776     7.0000    -0.1776     3.0144     3.0144     0.0000
  3   0     6.1898     6.0000    -0.1898     3.8789     3.8789    -0.0000
  4   0     0.9494     1.0000     0.0506     1.0010     1.0010    -0.0000
  5   0     0.8420     1.0000     0.1580     1.0297     1.0297     0.0000
  6   0     0.8939     1.0000     0.1061     0.9919     0.9919     0.0000
  7   0     0.8810     1.0000     0.1190     0.9737     0.9737     0.0000
  8   0     0.8845     1.0000     0.1155     0.9837     0.9837    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.046782
                0             6               2            7                1.188793
                0             6               4            1                0.983350
                2             7               3            6                0.918415
                2             7               5            1                0.949054
                3             6               6            1                0.977595
                3             6               7            1                0.971753
                3             6               8            1                0.976518
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.175605
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.2552404002
        Electronic Contribution:
                  0    
      0      -3.432877
      1      -1.022695
      2      -2.787590
        Nuclear Contribution:
                  0    
      0       4.729865
      1       1.612380
      2       3.666640
        Total Dipole moment:
                  0    
      0       1.296988
      1       0.589684
      2       0.879050
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      23.257414   0.395386   2.474028
      1       2.791041  23.423028   0.327977
      2       0.073299  -2.467686  23.119479
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.612232   0.289497  -0.735774
      1       0.561464  -0.496026  -0.662356
      2       0.556714   0.818626  -0.141141
 P Eigenvalues: 
                  0          1          2    
      0      20.634505  24.220702  24.944714
P(iso)  23.266640
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      25.012341   3.096233  -0.564173
      1       0.206111  29.525896  -5.893810
      2       1.701087  -2.733487  27.532201
 P Tensor eigenvectors:
                   0          1          2    
      0       0.607497  -0.789436  -0.087969
      1      -0.539089  -0.328421  -0.775579
      2      -0.583379  -0.518585   0.625091
 P Eigenvalues: 
                  0          1          2    
      0      22.983994  26.029121  33.057322
P(iso)  27.356812
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      29.629142   3.800735   2.117090
      1       2.954421  30.438707   0.251145
      2       2.799124   1.574993  27.366224
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.655384   0.361819   0.662992
      1       0.321792  -0.660378   0.678491
      2       0.683316   0.658018   0.316371
 P Eigenvalues: 
                  0          1          2    
      0      25.431612  27.765256  34.237205
P(iso)  29.144691
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      32.097487  -4.761317   1.694390
      1      -3.350440  27.623417   0.732375
      2       2.063548  -0.071005  26.543730
 P Tensor eigenvectors:
                   0          1          2    
      0       0.498821   0.149421  -0.853728
      1       0.677264   0.547457   0.491533
      2      -0.540825   0.823386  -0.171886
 P Eigenvalues: 
                  0          1          2    
      0      24.424400  27.088232  34.752001
P(iso)  28.754878
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.188361  -0.057988   0.649330
      1       0.073458  24.707118   1.143401
      2      -0.882648  -0.204172  33.861794
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.006905   0.999538  -0.029589
      1       0.999189   0.008070   0.039440
      2      -0.039660   0.029293   0.998784
 P Eigenvalues: 
                  0          1          2    
      0      24.684245  27.187442  33.885586
P(iso)  28.585758
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      51.440397 -37.678263   5.339275
      1     -29.550035 -20.061917  68.390954
      2      -1.165693  61.244156  21.887298
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.815135   0.077391   0.574078
      1      -0.344814   0.731508  -0.588216
      2      -0.465466  -0.677426  -0.569592
 P Eigenvalues: 
                  0          1          2    
      0      40.030069 -68.647416  81.883125
P(iso)  17.755259
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     165.809797  -1.413426  22.519533
      1      -3.701736 138.212846   5.658793
      2      21.641276   6.477848 164.146992
 P Tensor eigenvectors:
                   0          1          2    
      0       0.362626  -0.599352   0.713638
      1       0.825493   0.561964   0.052504
      2      -0.432508   0.570065   0.698544
 P Eigenvalues: 
                  0          1          2    
      0     133.902606 147.076722 187.190306
P(iso)  156.056545
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     149.528088  67.852327 -71.310240
      1      44.576224 210.413699  -7.543711
      2     -44.849988  23.526138  82.701688
 P Tensor eigenvectors:
                   0          1          2    
      0       0.591855   0.618061   0.517405
      1      -0.155289  -0.542449   0.825612
      2       0.790944  -0.568990  -0.225073
 P Eigenvalues: 
                  0          1          2    
      0      42.516588 153.898253 246.228634
P(iso)  147.547825
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -125.532406 -214.322534 -265.276377
      1     -235.811412 -66.260066 113.868836
      2     -236.398690 143.994086 -93.448663
 P Tensor eigenvectors:
                   0          1          2    
      0       0.137459   0.634025  -0.760997
      1       0.761523  -0.558941  -0.328128
      2      -0.633394  -0.534413  -0.559656
 P Eigenvalues: 
                  0          1          2    
      0     -206.106889 308.675998 -387.810244
P(iso)  -95.080378
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2126
          Total Spin-Spin Coupling ISO:  31.3028 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3543
          Total Spin-Spin Coupling ISO:  10.7569 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4818
          Total Spin-Spin Coupling ISO:  3.7905 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1061
          Total Spin-Spin Coupling ISO:  180.9535 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0133
          Total Spin-Spin Coupling ISO:  -0.8169 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.3405
          Total Spin-Spin Coupling ISO:  11.0863 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.9099
          Total Spin-Spin Coupling ISO:  0.1829 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.7646
          Total Spin-Spin Coupling ISO:  2.7788 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2791
          Total Spin-Spin Coupling ISO:  0.3354 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 3.6113
          Total Spin-Spin Coupling ISO:  0.5026 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0336
          Total Spin-Spin Coupling ISO:  -6.9294 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4982
          Total Spin-Spin Coupling ISO:  1.4545 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.3509
          Total Spin-Spin Coupling ISO:  0.0385 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.0411
          Total Spin-Spin Coupling ISO:  -0.1537 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.9326
          Total Spin-Spin Coupling ISO:  0.1271 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4523
          Total Spin-Spin Coupling ISO:  8.6011 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0486
          Total Spin-Spin Coupling ISO:  13.9538 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0075
          Total Spin-Spin Coupling ISO:  64.0278 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0765
          Total Spin-Spin Coupling ISO:  0.0682 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1209
          Total Spin-Spin Coupling ISO:  -0.0956 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1087
          Total Spin-Spin Coupling ISO:  -0.3512 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5992
          Total Spin-Spin Coupling ISO:  1.6335 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1368
          Total Spin-Spin Coupling ISO:  5.3479 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0874
          Total Spin-Spin Coupling ISO:  130.3164 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0925
          Total Spin-Spin Coupling ISO:  135.0906 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0907
          Total Spin-Spin Coupling ISO:  133.5907 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9393
          Total Spin-Spin Coupling ISO:  10.0007 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6322
          Total Spin-Spin Coupling ISO:  -0.5398 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.8266
          Total Spin-Spin Coupling ISO:  -0.2295 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.5587
          Total Spin-Spin Coupling ISO:  -0.0525 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.3113
          Total Spin-Spin Coupling ISO:  7.2256 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.8177
          Total Spin-Spin Coupling ISO:  1.1031 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9020
          Total Spin-Spin Coupling ISO:  4.6155 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7616
          Total Spin-Spin Coupling ISO:  -12.2225 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7619
          Total Spin-Spin Coupling ISO:  -13.1427 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7738
          Total Spin-Spin Coupling ISO:  -8.6238 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.289013915415   -0.470952315890    0.063711580939
               1 O      0.438841187349   -0.550351012828   -0.797264456860
               2 N      2.228928788716    0.500742664414    0.143600234713
               3 C      3.241911317681    0.575878154144    1.181526558579
               4 H      1.372914974083   -1.206036268271    0.885910956038
               5 H      2.195682249580    1.211167367652   -0.570020138114
               6 H      3.734331752191    1.543610791801    1.122383823586
               7 H      4.006637761857   -0.195928750900    1.066968783369
               8 H      2.798638053128    0.483809369877    2.173842657749
