-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1766182335
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000171274 
   Number of Beta  Electrons                 16.0000171274 
   Total number of  Electrons                32.0000342548 
   Exchange energy                          -21.4708231894 
   Correlation energy                        -1.2746695388 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7454927282 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1766182335 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8419     6.0000     0.1581     4.2601     4.2601    -0.0000
  1   0     8.3421     8.0000    -0.3421     2.1524     2.1524    -0.0000
  2   0     7.1715     7.0000    -0.1715     3.0323     3.0323    -0.0000
  3   0     6.1742     6.0000    -0.1742     3.8713     3.8713    -0.0000
  4   0     0.9525     1.0000     0.0475     1.0029     1.0029    -0.0000
  5   0     0.8440     1.0000     0.1560     1.0299     1.0299     0.0000
  6   0     0.8852     1.0000     0.1148     0.9714     0.9714    -0.0000
  7   0     0.9007     1.0000     0.0993     1.0015     1.0015     0.0000
  8   0     0.8880     1.0000     0.1120     0.9839     0.9839    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.045972
                0             6               2            7                1.193173
                0             6               4            1                0.981094
                2             7               3            6                0.928664
                2             7               5            1                0.947207
                3             6               6            1                0.968741
                3             6               7            1                0.976844
                3             6               8            1                0.974889
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.176618
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.2346942670
        Electronic Contribution:
                  0    
      0      -3.373522
      1      -1.333715
      2      -2.706310
        Nuclear Contribution:
                  0    
      0       4.751808
      1       1.648965
      2       3.587547
        Total Dipole moment:
                  0    
      0       1.378286
      1       0.315249
      2       0.881237
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      24.367710   0.930375   2.513594
      1      -2.541369  23.196219   0.799179
      2      -0.293483   2.290918  22.700654
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.360717  -0.260243  -0.895632
      1      -0.598486   0.801090   0.008269
      2       0.715330   0.539006  -0.444719
 P Eigenvalues: 
                  0          1          2    
      0      20.851505  24.490302  24.922775
P(iso)  23.421527
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.599317  -4.727291  -2.502905
      1      -1.654155  28.459731   3.969672
      2       0.771644   3.006880  26.057228
 P Tensor eigenvectors:
                   0          1          2    
      0       0.328887   0.808874  -0.487398
      1       0.647164   0.182824   0.740104
      2      -0.687759   0.558837   0.463346
 P Eigenvalues: 
                  0          1          2    
      0      23.158389  26.132992  32.824895
P(iso)  27.372092
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.461215   0.658178   3.239073
      1       0.705233  26.521943  -1.687642
      2       2.614256  -2.882145  33.804695
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.707584   0.645532   0.287425
      1       0.603498   0.763652  -0.229403
      2       0.367580  -0.011139   0.929925
 P Eigenvalues: 
                  0          1          2    
      0      24.357189  27.166433  35.264231
P(iso)  28.929285
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.749540   1.007472   1.625290
      1      -0.795129  31.624173   1.933167
      2       0.709022   2.345793  27.108706
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.626380   0.775668   0.077378
      1      -0.252456  -0.295773   0.921295
      2       0.737505   0.557546   0.381089
 P Eigenvalues: 
                  0          1          2    
      0      25.414109  27.542805  32.525505
P(iso)  28.494140
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      33.966373   1.906872  -0.612762
      1       3.208392  25.619333   0.224694
      2       0.322012   0.948072  27.466539
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.256074  -0.052601  -0.965225
      1       0.940318   0.217949  -0.261344
      2      -0.224117   0.974542   0.006349
 P Eigenvalues: 
                  0          1          2    
      0      24.763917  27.602135  34.686192
P(iso)  29.017415
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      13.892061  64.000567  22.402324
      1      58.052645  -5.513035 -42.875456
      2      14.029235 -38.592809  47.146136
 P Tensor eigenvectors:
                   0          1          2    
      0       0.791640  -0.353902  -0.498056
      1       0.022920   0.831793  -0.554613
      2       0.610558   0.427638   0.666592
 P Eigenvalues: 
                  0          1          2    
      0      44.961804 -63.874176  74.437534
P(iso)  18.508387
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     155.185638  17.751171  17.323345
      1      15.770320 146.772833   7.714129
      2      20.052948   9.078279 164.846655
 P Tensor eigenvectors:
                   0          1          2    
      0       0.693889   0.371654   0.616758
      1      -0.684216   0.607242   0.403864
      2      -0.224424  -0.702233   0.675650
 P Eigenvalues: 
                  0          1          2    
      0     132.576867 147.497430 186.730829
P(iso)  155.601709
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     203.204721 -85.554975 -49.968845
      1     -42.408317 130.139586 -43.479660
      2     -19.936318 -59.094236  93.413592
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.451402  -0.331330  -0.828527
      1      -0.550957  -0.626889   0.550869
      2      -0.701914   0.705146   0.100430
 P Eigenvalues: 
                  0          1          2    
      0      33.291555 154.995428 238.470916
P(iso)  142.252633
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -240.463022  69.660522 -209.634914
      1     113.288183 -19.539510 -217.655720
      2     -186.112024 -237.228123   7.402888
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.460272  -0.297981   0.836276
      1       0.788283  -0.570474   0.230586
      2       0.408363   0.765354   0.497467
 P Eigenvalues: 
                  0          1          2    
      0     -200.273297 296.588931 -348.915278
P(iso)  -84.199882
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2121
          Total Spin-Spin Coupling ISO:  31.0212 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3554
          Total Spin-Spin Coupling ISO:  10.7966 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4835
          Total Spin-Spin Coupling ISO:  4.0811 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1058
          Total Spin-Spin Coupling ISO:  182.2379 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0121
          Total Spin-Spin Coupling ISO:  -0.4748 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.1018
          Total Spin-Spin Coupling ISO:  0.5085 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.6034
          Total Spin-Spin Coupling ISO:  6.7280 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2751
          Total Spin-Spin Coupling ISO:  6.6683 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2798
          Total Spin-Spin Coupling ISO:  0.3085 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 3.6110
          Total Spin-Spin Coupling ISO:  0.6376 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0339
          Total Spin-Spin Coupling ISO:  -6.9246 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4955
          Total Spin-Spin Coupling ISO:  1.4957 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.1867
          Total Spin-Spin Coupling ISO:  -0.3109 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.8066
          Total Spin-Spin Coupling ISO:  0.5552 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.3037
          Total Spin-Spin Coupling ISO:  -0.1401 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4486
          Total Spin-Spin Coupling ISO:  8.6575 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0481
          Total Spin-Spin Coupling ISO:  13.9706 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0083
          Total Spin-Spin Coupling ISO:  63.1639 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1206
          Total Spin-Spin Coupling ISO:  0.2169 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0797
          Total Spin-Spin Coupling ISO:  -0.6869 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0954
          Total Spin-Spin Coupling ISO:  0.1582 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.6029
          Total Spin-Spin Coupling ISO:  1.7977 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1327
          Total Spin-Spin Coupling ISO:  5.2680 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0929
          Total Spin-Spin Coupling ISO:  136.4181 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0897
          Total Spin-Spin Coupling ISO:  128.3592 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0895
          Total Spin-Spin Coupling ISO:  134.1327 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9377
          Total Spin-Spin Coupling ISO:  9.2110 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.1702
          Total Spin-Spin Coupling ISO:  -0.5246 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2709
          Total Spin-Spin Coupling ISO:  0.0781 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.5043
          Total Spin-Spin Coupling ISO:  -0.6334 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.6683
          Total Spin-Spin Coupling ISO:  -0.5626 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9676
          Total Spin-Spin Coupling ISO:  9.5382 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.4159
          Total Spin-Spin Coupling ISO:  4.1231 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7644
          Total Spin-Spin Coupling ISO:  -11.3934 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7761
          Total Spin-Spin Coupling ISO:  -8.8400 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7610
          Total Spin-Spin Coupling ISO:  -13.2804 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.128296882428    0.192837046476   -0.010194001655
               1 O      0.376822545496   -0.319015457733   -0.811773645438
               2 N      2.391925944845   -0.222542570370    0.250055830097
               3 C      3.286127484077    0.406470697913    1.200457418863
               4 H      0.843137008589    1.071803932781    0.597142903965
               5 H      2.721984458142   -1.000946320037   -0.299278929689
               6 H      3.330510833251   -0.133589032619    2.149608600545
               7 H      2.936352019975    1.417623459651    1.406975482768
               8 H      4.291742823197    0.479298243938    0.787666340544
