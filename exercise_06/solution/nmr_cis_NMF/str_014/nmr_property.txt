-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1757137635
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999817708 
   Number of Beta  Electrons                 15.9999817708 
   Total number of  Electrons                31.9999635417 
   Exchange energy                          -21.4706287914 
   Correlation energy                        -1.2744994374 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7451282287 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1757137635 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8405     6.0000     0.1595     4.2593     4.2593    -0.0000
  1   0     8.3412     8.0000    -0.3412     2.1524     2.1524    -0.0000
  2   0     7.1769     7.0000    -0.1769     3.0163     3.0163    -0.0000
  3   0     6.1870     6.0000    -0.1870     3.8777     3.8777     0.0000
  4   0     0.9503     1.0000     0.0497     1.0013     1.0013    -0.0000
  5   0     0.8423     1.0000     0.1577     1.0298     1.0298     0.0000
  6   0     0.8871     1.0000     0.1129     0.9870     0.9870     0.0000
  7   0     0.8929     1.0000     0.1071     0.9909     0.9909     0.0000
  8   0     0.8817     1.0000     0.1183     0.9727     0.9727     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.046875
                0             6               2            7                1.188885
                0             6               4            1                0.983150
                2             7               3            6                0.919909
                2             7               5            1                0.948965
                3             6               6            1                0.977185
                3             6               7            1                0.977266
                3             6               8            1                0.970824
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.175714
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.2535485863
        Electronic Contribution:
                  0    
      0      -3.359926
      1      -1.166876
      2      -2.818976
        Nuclear Contribution:
                  0    
      0       4.738197
      1       1.637843
      2       3.642969
        Total Dipole moment:
                  0    
      0       1.378271
      1       0.470967
      2       0.823993
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      24.677894   1.240476   2.036154
      1       0.821651  20.950660   0.934865
      2      -2.190564  -0.352962  24.226574
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.250691  -0.061169  -0.966133
      1       0.964561   0.069086  -0.254657
      2      -0.082324   0.995734  -0.041682
 P Eigenvalues: 
                  0          1          2    
      0      20.659073  24.253348  24.942707
P(iso)  23.285042
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.776253   1.229699  -5.391881
      1       1.179633  23.320132  -1.498135
      2      -0.896939   0.284324  30.980653
 P Tensor eigenvectors:
                   0          1          2    
      0       0.268505   0.825726  -0.496066
      1      -0.962606   0.249246  -0.106146
      2       0.035994   0.506017   0.861772
 P Eigenvalues: 
                  0          1          2    
      0      23.009871  26.035027  33.032140
P(iso)  27.359013
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.752087  -0.463217   1.179547
      1      -1.559534  28.586518   4.319857
      2      -0.502299   3.821236  30.326839
 P Tensor eigenvectors:
                   0          1          2    
      0       0.453550   0.888363  -0.071436
      1       0.711879  -0.312887   0.628753
      2      -0.536210   0.336025   0.774316
 P Eigenvalues: 
                  0          1          2    
      0      24.784155  27.243778  33.637511
P(iso)  28.555148
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      32.085462   3.616620   0.339017
      1       3.795437  27.527851  -0.522999
      2       1.724667   0.446665  27.761625
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.491283   0.002909  -0.870995
      1       0.842553  -0.251896  -0.476082
      2       0.220785   0.967750  -0.121301
 P Eigenvalues: 
                  0          1          2    
      0      25.333138  27.765815  34.275986
P(iso)  29.124979
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.641684  -2.710921   4.384840
      1      -1.735921  29.343395  -2.264809
      2       3.676436  -3.299835  29.420255
 P Tensor eigenvectors:
                   0          1          2    
      0       0.782251  -0.343898   0.519440
      1      -0.004680  -0.837041  -0.547121
      2      -0.622946  -0.425555   0.656385
 P Eigenvalues: 
                  0          1          2    
      0      24.403713  27.073591  34.928031
P(iso)  28.801778
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0       0.202134 -15.719245  64.273304
      1     -14.936246  78.332911  10.392454
      2      52.087158   7.537295 -25.056116
 P Tensor eigenvectors:
                   0          1          2    
      0       0.866041  -0.465348  -0.182822
      1       0.191941  -0.028195   0.981001
      2       0.461662   0.884679  -0.064902
 P Eigenvalues: 
                  0          1          2    
      0      40.713226 -68.257409  81.023111
P(iso)  17.826310
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     154.641420   4.675143  24.857878
      1       1.845106 148.239411   6.233801
      2      25.805874   7.875770 165.142497
 P Tensor eigenvectors:
                   0          1          2    
      0       0.752883   0.234874   0.614818
      1       0.143911  -0.970299   0.194447
      2      -0.642228   0.057917   0.764323
 P Eigenvalues: 
                  0          1          2    
      0     133.745405 147.059608 187.218317
P(iso)  156.007776
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     217.006532  10.426824 -87.559546
      1      18.444138 156.664647 -31.894996
      2     -42.306979 -16.899410  66.971653
 P Tensor eigenvectors:
                   0          1          2    
      0       0.403543  -0.273362  -0.873170
      1       0.170946   0.960047  -0.221557
      2       0.898850  -0.059857   0.434150
 P Eigenvalues: 
                  0          1          2    
      0      41.413806 153.954577 245.274449
P(iso)  146.880944
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -296.629958 -168.511127 -86.177150
      1     -157.232396 261.198507 -66.375708
      2     -40.883105 -54.902382 -245.838758
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.532869  -0.278069  -0.799205
      1      -0.053816   0.953690  -0.295937
      2       0.844485  -0.114686  -0.523157
 P Eigenvalues: 
                  0          1          2    
      0     -205.444948 306.994105 -382.819366
P(iso)  -93.756736
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2124
          Total Spin-Spin Coupling ISO:  31.1585 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3544
          Total Spin-Spin Coupling ISO:  10.7046 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4812
          Total Spin-Spin Coupling ISO:  3.7728 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1060
          Total Spin-Spin Coupling ISO:  181.2278 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0135
          Total Spin-Spin Coupling ISO:  -0.8152 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.7295
          Total Spin-Spin Coupling ISO:  3.5981 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.3325
          Total Spin-Spin Coupling ISO:  10.5964 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.9488
          Total Spin-Spin Coupling ISO:  -0.2054 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2792
          Total Spin-Spin Coupling ISO:  0.3299 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 3.6107
          Total Spin-Spin Coupling ISO:  0.5213 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0337
          Total Spin-Spin Coupling ISO:  -6.9398 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4986
          Total Spin-Spin Coupling ISO:  1.4799 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.9047
          Total Spin-Spin Coupling ISO:  0.2145 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.3451
          Total Spin-Spin Coupling ISO:  0.0197 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.0714
          Total Spin-Spin Coupling ISO:  -0.1995 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4519
          Total Spin-Spin Coupling ISO:  8.5709 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0483
          Total Spin-Spin Coupling ISO:  13.9260 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0076
          Total Spin-Spin Coupling ISO:  63.9576 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1036
          Total Spin-Spin Coupling ISO:  -0.4332 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0777
          Total Spin-Spin Coupling ISO:  0.0621 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1239
          Total Spin-Spin Coupling ISO:  0.0011 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5977
          Total Spin-Spin Coupling ISO:  1.5927 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1368
          Total Spin-Spin Coupling ISO:  5.3314 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0904
          Total Spin-Spin Coupling ISO:  132.7050 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0878
          Total Spin-Spin Coupling ISO:  130.1023 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0925
          Total Spin-Spin Coupling ISO:  136.6267 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9391
          Total Spin-Spin Coupling ISO:  9.8614 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.4961
          Total Spin-Spin Coupling ISO:  -0.0771 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6167
          Total Spin-Spin Coupling ISO:  -0.5894 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.8943
          Total Spin-Spin Coupling ISO:  -0.3180 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9160
          Total Spin-Spin Coupling ISO:  5.5980 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.3213
          Total Spin-Spin Coupling ISO:  6.9300 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.7954
          Total Spin-Spin Coupling ISO:  0.4739 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7558
          Total Spin-Spin Coupling ISO:  -13.6222 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7762
          Total Spin-Spin Coupling ISO:  -8.5781 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7653
          Total Spin-Spin Coupling ISO:  -11.5570 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.113257222923   -0.173433603803    0.163440859069
               1 O      0.375242384722   -0.448240943182   -0.758405182244
               2 N      2.416577449189    0.174496606509    0.041661010563
               3 C      3.283769371598    0.499030810413    1.160051306492
               4 H      0.776915909982   -0.179661519810    1.217028971170
               5 H      2.778111310407    0.222606182751   -0.897575625977
               6 H      2.779504601565    1.163264816077    1.862556629779
               7 H      4.159959740611    1.024487904667    0.786502939627
               8 H      3.623562009002   -0.390610253623    1.695399091521
