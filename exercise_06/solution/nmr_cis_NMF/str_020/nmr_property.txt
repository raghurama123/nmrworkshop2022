-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1768558968
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999986524 
   Number of Beta  Electrons                 15.9999986524 
   Total number of  Electrons                31.9999973047 
   Exchange energy                          -21.4710368277 
   Correlation energy                        -1.2747128629 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7457496907 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1768558968 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8431     6.0000     0.1569     4.2610     4.2610     0.0000
  1   0     8.3423     8.0000    -0.3423     2.1523     2.1523     0.0000
  2   0     7.1700     7.0000    -0.1700     3.0367     3.0367     0.0000
  3   0     6.1719     6.0000    -0.1719     3.8703     3.8703    -0.0000
  4   0     0.9524     1.0000     0.0476     1.0033     1.0033     0.0000
  5   0     0.8444     1.0000     0.1556     1.0297     1.0297     0.0000
  6   0     0.9028     1.0000     0.0972     1.0040     1.0040     0.0000
  7   0     0.8856     1.0000     0.1144     0.9726     0.9726    -0.0000
  8   0     0.8875     1.0000     0.1125     0.9813     0.9813    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.045747
                0             6               2            7                1.194754
                0             6               4            1                0.980469
                2             7               3            6                0.930739
                2             7               5            1                0.946573
                3             6               6            1                0.976045
                3             6               7            1                0.969252
                3             6               8            1                0.973851
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.176856
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.2340252605
        Electronic Contribution:
                  0    
      0      -3.345756
      1      -1.210551
      2      -2.795876
        Nuclear Contribution:
                  0    
      0       4.728654
      1       1.574627
      2       3.650157
        Total Dipole moment:
                  0    
      0       1.382898
      1       0.364077
      2       0.854281
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      24.863446   1.086879   2.296551
      1      -0.637563  21.407800   1.312178
      2      -2.163762   1.249503  24.089977
 P Tensor eigenvectors:
                   0          1          2    
      0       0.044695  -0.443632   0.895094
      1      -0.927444   0.314572   0.202220
      2       0.371283   0.839187   0.397384
 P Eigenvalues: 
                  0          1          2    
      0      20.885923  24.540892  24.934408
P(iso)  23.453741
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.578833  -2.218675  -4.960612
      1      -0.210079  24.362288   2.828692
      2      -0.952467   2.462699  29.181759
 P Tensor eigenvectors:
                   0          1          2    
      0       0.017280   0.818181  -0.574702
      1      -0.912590   0.247748   0.325269
      2       0.408510   0.518847   0.750945
 P Eigenvalues: 
                  0          1          2    
      0      23.191740  26.160030  32.771110
P(iso)  27.374294
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.472810   0.697764   0.399446
      1       0.250687  25.893784   0.947756
      2      -1.590005   1.076937  32.134015
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.285766   0.950413  -0.122691
      1       0.941759   0.302207   0.147518
      2      -0.177281   0.073390   0.981420
 P Eigenvalues: 
                  0          1          2    
      0      25.565253  27.587983  32.347373
P(iso)  28.500203
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      28.197410   4.609211   1.584295
      1       4.516034  32.045722  -0.368766
      2       1.934976   1.016732  26.607376
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.741079  -0.367209   0.562103
      1       0.424949   0.391645   0.816108
      2       0.519827  -0.843665   0.134195
 P Eigenvalues: 
                  0          1          2    
      0      24.376353  27.219769  35.254386
P(iso)  28.950169
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      31.160708  -2.809237   3.463764
      1      -2.594446  28.253236   0.037286
      2       4.579227  -1.214007  27.560715
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.582911   0.130226   0.802032
      1      -0.326782   0.866155  -0.378140
      2       0.743927   0.482512   0.462336
 P Eigenvalues: 
                  0          1          2    
      0      24.626844  27.545398  34.802417
P(iso)  28.991553
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0      -5.981411  22.404320  65.126256
      1      16.798543  62.593188 -29.578524
      2      56.135965 -30.250011  -0.459534
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.864305  -0.466339   0.188428
      1      -0.320340   0.221564  -0.921027
      2      -0.387762   0.856410   0.340886
 P Eigenvalues: 
                  0          1          2    
      0      45.948712 -62.397477  72.601007
P(iso)  18.717414
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     151.900956   9.238163  24.536606
      1      11.499913 151.631449   8.636069
      2      23.175218   7.395853 162.937368
 P Tensor eigenvectors:
                   0          1          2    
      0       0.798410   0.054223   0.599667
      1      -0.193203  -0.920203   0.340441
      2      -0.570275   0.387669   0.724223
 P Eigenvalues: 
                  0          1          2    
      0     132.323674 147.580426 186.565674
P(iso)  155.489925
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     216.907543 -32.904507 -86.132045
      1     -15.674353 138.801022 -37.482133
      2     -32.873130 -37.976887  67.764962
 P Tensor eigenvectors:
                   0          1          2    
      0       0.419187  -0.020049  -0.907679
      1       0.361334   0.920851   0.146533
      2       0.832899  -0.389400   0.393253
 P Eigenvalues: 
                  0          1          2    
      0      31.629285 155.086873 236.757369
P(iso)  141.157842
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -305.365615 -56.874089 -77.263244
      1     -43.560004 220.489140 -192.222155
      2     -25.209726 -189.022119 -161.135574
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.565144   0.002549  -0.824989
      1       0.272604   0.944402  -0.183825
      2       0.778653  -0.328783  -0.534418
 P Eigenvalues: 
                  0          1          2    
      0     -199.199318 297.454476 -344.267208
P(iso)  -82.004017
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2120
          Total Spin-Spin Coupling ISO:  30.9260 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3554
          Total Spin-Spin Coupling ISO:  10.8047 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4837
          Total Spin-Spin Coupling ISO:  4.1202 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1056
          Total Spin-Spin Coupling ISO:  182.7931 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0123
          Total Spin-Spin Coupling ISO:  -0.4696 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5889
          Total Spin-Spin Coupling ISO:  7.0124 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.1396
          Total Spin-Spin Coupling ISO:  1.2092 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.2516
          Total Spin-Spin Coupling ISO:  5.5404 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2799
          Total Spin-Spin Coupling ISO:  0.3131 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 3.6109
          Total Spin-Spin Coupling ISO:  0.6550 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0337
          Total Spin-Spin Coupling ISO:  -6.9660 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4957
          Total Spin-Spin Coupling ISO:  1.5215 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.7963
          Total Spin-Spin Coupling ISO:  0.6223 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.2134
          Total Spin-Spin Coupling ISO:  -0.3077 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.2867
          Total Spin-Spin Coupling ISO:  -0.1903 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4479
          Total Spin-Spin Coupling ISO:  8.6595 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0477
          Total Spin-Spin Coupling ISO:  13.9706 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0084
          Total Spin-Spin Coupling ISO:  63.0495 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0768
          Total Spin-Spin Coupling ISO:  -0.6947 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1186
          Total Spin-Spin Coupling ISO:  0.2448 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0976
          Total Spin-Spin Coupling ISO:  0.1214 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.6036
          Total Spin-Spin Coupling ISO:  1.7693 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1315
          Total Spin-Spin Coupling ISO:  5.2349 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0893
          Total Spin-Spin Coupling ISO:  128.6236 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0925
          Total Spin-Spin Coupling ISO:  136.9206 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0906
          Total Spin-Spin Coupling ISO:  133.3260 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9377
          Total Spin-Spin Coupling ISO:  9.1200 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.2427
          Total Spin-Spin Coupling ISO:  0.0148 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.2406
          Total Spin-Spin Coupling ISO:  -0.5681 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.4616
          Total Spin-Spin Coupling ISO:  -0.6328 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9760
          Total Spin-Spin Coupling ISO:  10.2065 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.6245
          Total Spin-Spin Coupling ISO:  -0.2673 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.4474
          Total Spin-Spin Coupling ISO:  3.1990 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7709
          Total Spin-Spin Coupling ISO:  -11.1778 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7590
          Total Spin-Spin Coupling ISO:  -13.2037 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7729
          Total Spin-Spin Coupling ISO:  -8.7997 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.088293404694   -0.008481760612    0.129745833405
               1 O      0.364262397934   -0.397136125110   -0.761122784065
               2 N      2.442416583530    0.019848860652    0.078626223509
               3 C      3.298776403051    0.476990566244    1.152865104007
               4 H      0.698183139382    0.367411551407    1.093570475254
               5 H      2.859219351781   -0.335748928889   -0.767990381907
               6 H      2.710029788147    0.562346817850    2.065334978459
               7 H      3.749046184685    1.448910830428    0.938203213574
               8 H      4.096672746797   -0.242201811969    1.341427337765
