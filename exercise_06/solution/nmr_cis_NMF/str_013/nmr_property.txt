-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1755101192
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999769706 
   Number of Beta  Electrons                 15.9999769706 
   Total number of  Electrons                31.9999539412 
   Exchange energy                          -21.4705471147 
   Correlation energy                        -1.2744566531 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7450037678 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1755101192 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8413     6.0000     0.1587     4.2596     4.2596     0.0000
  1   0     8.3408     8.0000    -0.3408     2.1524     2.1524     0.0000
  2   0     7.1778     7.0000    -0.1778     3.0128     3.0128     0.0000
  3   0     6.1923     6.0000    -0.1923     3.8795     3.8795    -0.0000
  4   0     0.9486     1.0000     0.0514     1.0008     1.0008     0.0000
  5   0     0.8420     1.0000     0.1580     1.0297     1.0297     0.0000
  6   0     0.8822     1.0000     0.1178     0.9802     0.9802    -0.0000
  7   0     0.8944     1.0000     0.1056     0.9925     0.9925    -0.0000
  8   0     0.8805     1.0000     0.1195     0.9757     0.9757    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.046919
                0             6               2            7                1.188354
                0             6               4            1                0.983387
                2             7               3            6                0.917436
                2             7               5            1                0.949320
                3             6               6            1                0.975272
                3             6               7            1                0.977710
                3             6               8            1                0.973105
# -----------------------------------------------------------
$ Calculation_Info
   description: Details of the calculation
   geom. index: 1
   prop. index: 1
     Multiplicity:                          1
     Charge:                                0
     number of atoms:                       9
     number of electrons:                   0
     number of frozen core electrons:       1
     number of correlated electrons:        0
     number of basis functions:             194
     number of aux C basis functions:       0
     number of aux J basis functions:       251
     number of aux JK basis functions:      0
     number of aux CABS basis functions:    0
     Total Energy                           -209.175510
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : nmr.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        4.2551146945
        Electronic Contribution:
                  0    
      0      -3.366002
      1      -1.145067
      2      -2.823074
        Nuclear Contribution:
                  0    
      0       4.745086
      1       1.616555
      2       3.646661
        Total Dipole moment:
                  0    
      0       1.379084
      1       0.471487
      2       0.823587
# -----------------------------------------------------------
$ EPRNMR_OrbitalShielding
   description: Orbital contribution to the NMR Shielding tensor (ppm)
   geom. index: 1
   prop. index: 1
 Number of stored nuclei 9
 Source density: 1 SCF 
 Nucleus: 4 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      24.539889   1.302686   2.020804
      1       1.222551  21.021669   0.745156
      2      -2.118061  -0.657875  24.191043
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.306816  -0.005397  -0.951754
      1       0.951649   0.014147  -0.306862
      2      -0.015120   0.999885  -0.000796
 P Eigenvalues: 
                  0          1          2    
      0      20.614557  24.191985  24.946059
P(iso)  23.250867
 Nucleus: 5 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.557608   1.542285  -5.329811
      1       1.462319  23.472277  -1.857016
      2      -0.760988  -0.177083  31.037381
 P Tensor eigenvectors:
                   0          1          2    
      0       0.311656   0.821608  -0.477316
      1      -0.950195   0.268950  -0.157468
      2      -0.001002   0.502619   0.864508
 P Eigenvalues: 
                  0          1          2    
      0      22.979291  26.014912  33.073062
P(iso)  27.355755
 Nucleus: 6 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      26.438164  -0.346004   1.495827
      1      -1.463441  29.291901   4.545356
      2      -0.076449   4.265364  30.157052
 P Tensor eigenvectors:
                   0          1          2    
      0       0.525996   0.850071  -0.026598
      1       0.640216  -0.375169   0.670352
      2      -0.559868   0.369631   0.741566
 P Eigenvalues: 
                  0          1          2    
      0      24.588133  27.149170  34.149814
P(iso)  28.629039
 Nucleus: 7 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      32.857298   2.999189   0.109443
      1       3.027632  26.747274  -0.192513
      2       1.654026   0.499290  27.875973
 P Tensor eigenvectors:
                   0          1          2    
      0       0.384747   0.082250   0.919350
      1      -0.919293   0.123584   0.373667
      2      -0.082883  -0.988920   0.123161
 P Eigenvalues: 
                  0          1          2    
      0      25.494977  27.772764  34.212804
P(iso)  29.160182
 Nucleus: 8 H 
 Shielding tensor (ppm): 
                  0          1          2    
      0      27.086400  -2.336161   4.228123
      1      -1.212901  29.337435  -2.506185
      2       3.127901  -3.382385  29.704621
 P Tensor eigenvectors:
                   0          1          2    
      0       0.800083  -0.386293   0.458960
      1      -0.078298  -0.825776  -0.558537
      2      -0.594757  -0.410941   0.690935
 P Eigenvalues: 
                  0          1          2    
      0      24.474067  27.100883  34.553506
P(iso)  28.709485
 Nucleus: 0 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0       4.559629 -24.216701  61.449049
      1     -23.813676  74.963607  19.085818
      2      49.370446  15.268740 -26.606378
 P Tensor eigenvectors:
                   0          1          2    
      0       0.847824  -0.445604  -0.287457
      1       0.258531  -0.125952   0.957757
      2       0.462986   0.886326  -0.008417
 P Eigenvalues: 
                  0          1          2    
      0      39.669115 -69.105690  82.353433
P(iso)  17.638953
 Nucleus: 3 C 
 Shielding tensor (ppm): 
                  0          1          2    
      0     155.238823   3.145517  24.369506
      1       2.175401 147.944368   7.502663
      2      25.606528   8.388131 165.162927
 P Tensor eigenvectors:
                   0          1          2    
      0       0.728517   0.302277   0.614729
      1       0.228353  -0.953212   0.198096
      2      -0.645847   0.003941   0.763457
 P Eigenvalues: 
                  0          1          2    
      0     134.025330 147.077842 187.242947
P(iso)  156.115373
 Nucleus: 2 N 
 Shielding tensor (ppm): 
                  0          1          2    
      0     215.821682  19.213898 -86.371393
      1      21.239703 160.329580 -29.755883
      2     -42.825806 -14.622292  67.877296
 P Tensor eigenvectors:
                   0          1          2    
      0       0.402888  -0.314195  -0.859629
      1       0.140289   0.949330  -0.281231
      2       0.904434  -0.007292   0.426552
 P Eigenvalues: 
                  0          1          2    
      0      43.289093 153.747671 246.991794
P(iso)  148.009519
 Nucleus: 1 O 
 Shielding tensor (ppm): 
                  0          1          2    
      0     -283.641371 -194.461712 -95.893436
      1     -191.226292 247.164592 -38.459802
      2     -51.602366 -24.395002 -252.127064
 P Tensor eigenvectors:
                   0          1          2    
      0      -0.508855  -0.316423  -0.800589
      1      -0.147271   0.948284  -0.281191
      2       0.848161  -0.025182  -0.529139
 P Eigenvalues: 
                  0          1          2    
      0     -206.852410 309.756596 -391.508030
P(iso)  -96.201281
# -----------------------------------------------------------
$ EPRNMR_SSCoupling
   description: Spin-Spin couplings
   geom. index: 1
   prop. index: 1
 Source density: 0 UNKNOWN 
 Number of nuclei pairs to calculate something:              36
 Number of nuclei pairs to calculate DSO   terms:            36
 Number of nuclei pairs to calculate PSO   terms:            36
 Number of nuclei pairs to calculate FC    terms:            36
 Number of nuclei pairs to calculate SD    terms:            36
 Number of nuclei pairs to calculate PSO   perturbations:    8
 Number of nuclei pairs to calculate SD/FC perturbations:    8
 Information for pairs 
 Pair: 0 Index A: 0 Atomic Number A: 6 Index B: 1 Atomic Number B: 8
          gn_A:  1.4048,  gn_B: -0.7575 Pair Distance: 1.2125
          Total Spin-Spin Coupling ISO:  31.2136 
 Pair: 1 Index A: 0 Atomic Number A: 6 Index B: 2 Atomic Number B: 7
          gn_A:  1.4048,  gn_B: 0.4038 Pair Distance: 1.3542
          Total Spin-Spin Coupling ISO:  10.7217 
 Pair: 2 Index A: 0 Atomic Number A: 6 Index B: 3 Atomic Number B: 6
          gn_A:  1.4048,  gn_B: 1.4048 Pair Distance: 2.4812
          Total Spin-Spin Coupling ISO:  3.7030 
 Pair: 3 Index A: 0 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.1061
          Total Spin-Spin Coupling ISO:  180.8288 
 Pair: 4 Index A: 0 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.0134
          Total Spin-Spin Coupling ISO:  -0.8772 
 Pair: 5 Index A: 0 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.8031
          Total Spin-Spin Coupling ISO:  1.9146 
 Pair: 6 Index A: 0 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 3.3437
          Total Spin-Spin Coupling ISO:  11.3796 
 Pair: 7 Index A: 0 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.8698
          Total Spin-Spin Coupling ISO:  0.6991 
 Pair: 8 Index A: 1 Atomic Number A: 8 Index B: 2 Atomic Number B: 7
          gn_A:  -0.7575,  gn_B: 0.4038 Pair Distance: 2.2789
          Total Spin-Spin Coupling ISO:  0.3323 
 Pair: 9 Index A: 1 Atomic Number A: 8 Index B: 3 Atomic Number B: 6
          gn_A:  -0.7575,  gn_B: 1.4048 Pair Distance: 3.6109
          Total Spin-Spin Coupling ISO:  0.4912 
 Pair: 10 Index A: 1 Atomic Number A: 8 Index B: 4 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.0336
          Total Spin-Spin Coupling ISO:  -6.9279 
 Pair: 11 Index A: 1 Atomic Number A: 8 Index B: 5 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 2.4982
          Total Spin-Spin Coupling ISO:  1.4707 
 Pair: 12 Index A: 1 Atomic Number A: 8 Index B: 6 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 3.9613
          Total Spin-Spin Coupling ISO:  0.0458 
 Pair: 13 Index A: 1 Atomic Number A: 8 Index B: 7 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.3529
          Total Spin-Spin Coupling ISO:  0.0554 
 Pair: 14 Index A: 1 Atomic Number A: 8 Index B: 8 Atomic Number B: 1
          gn_A:  -0.7575,  gn_B: 5.5857 Pair Distance: 4.0112
          Total Spin-Spin Coupling ISO:  -0.0855 
 Pair: 15 Index A: 2 Atomic Number A: 7 Index B: 3 Atomic Number B: 6
          gn_A:  0.4038,  gn_B: 1.4048 Pair Distance: 1.4527
          Total Spin-Spin Coupling ISO:  8.5513 
 Pair: 16 Index A: 2 Atomic Number A: 7 Index B: 4 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0488
          Total Spin-Spin Coupling ISO:  13.9253 
 Pair: 17 Index A: 2 Atomic Number A: 7 Index B: 5 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 1.0074
          Total Spin-Spin Coupling ISO:  64.0995 
 Pair: 18 Index A: 2 Atomic Number A: 7 Index B: 6 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1129
          Total Spin-Spin Coupling ISO:  -0.2928 
 Pair: 19 Index A: 2 Atomic Number A: 7 Index B: 7 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.0750
          Total Spin-Spin Coupling ISO:  0.0629 
 Pair: 20 Index A: 2 Atomic Number A: 7 Index B: 8 Atomic Number B: 1
          gn_A:  0.4038,  gn_B: 5.5857 Pair Distance: 2.1191
          Total Spin-Spin Coupling ISO:  -0.1511 
 Pair: 21 Index A: 3 Atomic Number A: 6 Index B: 4 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.5981
          Total Spin-Spin Coupling ISO:  1.5610 
 Pair: 22 Index A: 3 Atomic Number A: 6 Index B: 5 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 2.1377
          Total Spin-Spin Coupling ISO:  5.3403 
 Pair: 23 Index A: 3 Atomic Number A: 6 Index B: 6 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0912
          Total Spin-Spin Coupling ISO:  134.0392 
 Pair: 24 Index A: 3 Atomic Number A: 6 Index B: 7 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0873
          Total Spin-Spin Coupling ISO:  130.0252 
 Pair: 25 Index A: 3 Atomic Number A: 6 Index B: 8 Atomic Number B: 1
          gn_A:  1.4048,  gn_B: 5.5857 Pair Distance: 1.0919
          Total Spin-Spin Coupling ISO:  135.2673 
 Pair: 26 Index A: 4 Atomic Number A: 1 Index B: 5 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.9396
          Total Spin-Spin Coupling ISO:  10.0375 
 Pair: 27 Index A: 4 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.6299
          Total Spin-Spin Coupling ISO:  -0.1322 
 Pair: 28 Index A: 4 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 3.6391
          Total Spin-Spin Coupling ISO:  -0.5740 
 Pair: 29 Index A: 4 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.7524
          Total Spin-Spin Coupling ISO:  -0.2053 
 Pair: 30 Index A: 5 Atomic Number A: 1 Index B: 6 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.8813
          Total Spin-Spin Coupling ISO:  3.5573 
 Pair: 31 Index A: 5 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.3052
          Total Spin-Spin Coupling ISO:  7.4429 
 Pair: 32 Index A: 5 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 2.8440
          Total Spin-Spin Coupling ISO:  1.9243 
 Pair: 33 Index A: 6 Atomic Number A: 1 Index B: 7 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7591
          Total Spin-Spin Coupling ISO:  -13.1778 
 Pair: 34 Index A: 6 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7751
          Total Spin-Spin Coupling ISO:  -8.2962 
 Pair: 35 Index A: 7 Atomic Number A: 1 Index B: 8 Atomic Number B: 1
          gn_A:  5.5857,  gn_B: 5.5857 Pair Distance: 1.7625
          Total Spin-Spin Coupling ISO:  -12.4448 
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.122650800664   -0.204663843455    0.163946655754
               1 O      0.379818809357   -0.460345585025   -0.759682997492
               2 N      2.404477563477    0.214173678915    0.039780643236
               3 C      3.278365123254    0.513076318217    1.161043154697
               4 H      0.808952260267   -0.292358584914    1.221012991424
               5 H      2.748308270784    0.321638131036   -0.901012075446
               6 H      2.841503979716    1.265426247079    1.819734835716
               7 H      4.213835453662    0.912863017988    0.777256947217
               8 H      3.508987738818   -0.377869379841    1.748579844893
