! B3LYP def2-TZVPP def2/J RIJCOSX NMR

* xyzFILE 0 1 geom.xyz

%MaxCore 32000

%pal nprocs 8
end

%EPRNMR
     NUCLEI = ALL {SHIFT, SSALL}
END
