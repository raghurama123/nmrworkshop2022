-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:      -79.7991355604
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                  8.9999966026 
   Number of Beta  Electrons                  8.9999966026 
   Total number of  Electrons                17.9999932051 
   Exchange energy                           -9.8648638546 
   Correlation energy                        -0.6534375594 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -10.5183014140 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -79.7991355604 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 8
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 7
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.2320     6.0000    -0.2320     3.8895     3.8895    -0.0000
  1   0     6.2320     6.0000    -0.2320     3.8895     3.8895     0.0000
  2   0     0.9226     1.0000     0.0774     0.9937     0.9937    -0.0000
  3   0     0.9228     1.0000     0.0772     0.9938     0.9938    -0.0000
  4   0     0.9226     1.0000     0.0774     0.9937     0.9937    -0.0000
  5   0     0.9226     1.0000     0.0774     0.9937     0.9937     0.0000
  6   0     0.9228     1.0000     0.0772     0.9938     0.9938     0.0000
  7   0     0.9226     1.0000     0.0774     0.9937     0.9937    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                0.937146
                0             6               2            1                0.984818
                0             6               3            1                0.984913
                0             6               4            1                0.984831
                1             6               5            1                0.984832
                1             6               6            1                0.984911
                1             6               7            1                0.984818
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0054279203
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:      -79.8011111587
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                  8.9999958123 
   Number of Beta  Electrons                  8.9999958123 
   Total number of  Electrons                17.9999916246 
   Exchange energy                           -9.8913349328 
   Correlation energy                        -0.6549185988 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -10.5462535316 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -79.8011111587 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0054238752
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:      -79.8012097554
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                  8.9999961994 
   Number of Beta  Electrons                  8.9999961994 
   Total number of  Electrons                17.9999923988 
   Exchange energy                           -9.8927211721 
   Correlation energy                        -0.6549781823 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -10.5476993543 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -79.8012097554 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0054178208
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:      -79.8012430709
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                  8.9999968026 
   Number of Beta  Electrons                  8.9999968026 
   Total number of  Electrons                17.9999936052 
   Exchange energy                           -9.8923090808 
   Correlation energy                        -0.6549618381 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -10.5472709189 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -79.8012430709 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0054141101
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:      -79.8012474724
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                  8.9999970846 
   Number of Beta  Electrons                  8.9999970846 
   Total number of  Electrons                17.9999941692 
   Exchange energy                           -9.8922019258 
   Correlation energy                        -0.6549706333 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -10.5471725591 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -79.8012474724 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0054139188
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:      -79.8012473702
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                  8.9999971253 
   Number of Beta  Electrons                  8.9999971253 
   Total number of  Electrons                17.9999942505 
   Exchange energy                           -9.8922453298 
   Correlation energy                        -0.6549782269 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -10.5472235567 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -79.8012473702 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 6
   prop. index: 1
     Number of atoms                     : 8
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 7
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.2445     6.0000    -0.2445     3.8973     3.8973    -0.0000
  1   0     6.2445     6.0000    -0.2445     3.8973     3.8973    -0.0000
  2   0     0.9184     1.0000     0.0816     0.9939     0.9939    -0.0000
  3   0     0.9186     1.0000     0.0814     0.9940     0.9940     0.0000
  4   0     0.9184     1.0000     0.0816     0.9939     0.9939    -0.0000
  5   0     0.9184     1.0000     0.0816     0.9939     0.9939     0.0000
  6   0     0.9187     1.0000     0.0813     0.9940     0.9940     0.0000
  7   0     0.9184     1.0000     0.0816     0.9939     0.9939    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            6                0.941307
                0             6               2            1                0.985642
                0             6               3            1                0.985724
                0             6               4            1                0.985653
                1             6               5            1                0.985654
                1             6               6            1                0.985722
                1             6               7            1                0.985642
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0054142617
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 6
   prop. index: 1
       Filename                          : opt.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000007270
        Electronic Contribution:
                  0    
      0      -0.000000
      1      -0.000000
      2      -0.000000
        Nuclear Contribution:
                  0    
      0      -0.000000
      1      -0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0      -0.000000
      1      -0.000000
      2      -0.000000
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    8 
    Geometry Index:     1 
    Coordinates:
               0 C      0.990140000000    0.069610000000    0.071060000000
               1 C      2.509330000000    0.069610000000    0.071060000000
               2 H      0.613700000000    0.349300000000   -0.935440000000
               3 H      0.613700000000    0.801420000000    0.816520000000
               4 H      0.613700000000   -0.941890000000    0.332090000000
               5 H      2.885770000000    1.081110000000   -0.189980000000
               6 H      2.885770000000   -0.662200000000   -0.674410000000
               7 H      2.885770000000   -0.210080000000    1.077550000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    8 
    Geometry Index:     2 
    Coordinates:
               0 C      0.984601063900    0.069610687306    0.071054611395
               1 C      2.514868938583    0.069609302843    0.071061015034
               2 H      0.600025761947    0.343133199871   -0.913216580470
               3 H      0.600022264434    0.785252724532    0.800054519410
               4 H      0.600026897028   -0.919553072852    0.326337187080
               5 H      2.899442568251    1.058771920707   -0.184227719458
               6 H      2.899446440190   -0.646029997478   -0.657943200137
               7 H      2.899446065666   -0.203914764929    1.055330167146
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    8 
    Geometry Index:     3 
    Coordinates:
               0 C      0.983976851301    0.069613554565    0.071052883613
               1 C      2.515493157452    0.069606445177    0.071061118025
               2 H      0.593322217179    0.342153943147   -0.909700993687
               3 H      0.593319677124    0.782694000810    0.797451878082
               4 H      0.593325230664   -0.916015961351    0.325425108681
               5 H      2.906144402162    1.055235340767   -0.183314054510
               6 H      2.906149469794   -0.643472145989   -0.655340162730
               7 H      2.906148994323   -0.202935177126    1.051814222525
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    8 
    Geometry Index:     4 
    Coordinates:
               0 C      0.984979265159    0.069618004879    0.071053343857
               1 C      2.514490740370    0.069602005417    0.071059461888
               2 H      0.589359313746    0.341778299280   -0.908362567833
               3 H      0.589356474934    0.781718810565    0.796462563013
               4 H      0.589363731752   -0.914666710968    0.325076363691
               5 H      2.910106151397    1.053886729630   -0.182963703806
               6 H      2.910113249704   -0.642498107788   -0.654350561106
               7 H      2.910111072938   -0.202559031015    1.050475100296
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    8 
    Geometry Index:     5 
    Coordinates:
               0 C      0.985995466564    0.069620862908    0.071054984491
               1 C      2.513474536788    0.069599141389    0.071057461857
               2 H      0.588910924341    0.341740825113   -0.908236041628
               3 H      0.588906331076    0.781626812526    0.796370525580
               4 H      0.588915380932   -0.914538009791    0.325042190432
               5 H      2.910554582567    1.053758226337   -0.182928988197
               6 H      2.910563665341   -0.642406429055   -0.654258473992
               7 H      2.910559112391   -0.202521429428    1.050348341458
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    8 
    Geometry Index:     6 
    Coordinates:
               0 C      0.986284219750    0.069621847155    0.071056000640
               1 C      2.513185787624    0.069598163562    0.071056463764
               2 H      0.589164939523    0.341759875565   -0.908308049850
               3 H      0.589159382474    0.781679675901    0.796424628463
               4 H      0.589169130075   -0.914610225513    0.325060118391
               5 H      2.910300868760    1.053830435489   -0.182946905430
               6 H      2.910310634308   -0.642459237204   -0.654312607735
               7 H      2.910305037485   -0.202540534955    1.050420351756
