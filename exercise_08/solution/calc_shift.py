import numpy as np

# Isotropic shielding of TMS
tms_H=31.7

# Isotropic shielding of methane
H=[31.489, 31.493, 31.491, 31.492]
sigma = np.mean(np.array(H))
delta = tms_H - sigma
print("%5.2f %5.2f" % (sigma, delta))

# Isotropic shielding of ethane
H = [ 30.780, 30.781, 30.780, 30.779, 30.779, 30.779 ]
sigma = np.mean(np.array(H))
delta = tms_H - sigma
print("%5.2f %5.2f" % (sigma, delta))

# Isotropic shielding of ethylene
H = [25.974, 25.974, 25.975, 25.975]
sigma = np.mean(np.array(H))
delta = tms_H - sigma
print("%5.2f %5.2f" % (sigma, delta))

# Isotropic shielding of acetylene
H = [30.525, 30.525]
sigma = np.mean(np.array(H))
delta = tms_H - sigma
print("%5.2f %5.2f" % (sigma, delta))

