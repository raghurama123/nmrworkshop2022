-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -209.1741545163
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 15.9999969854 
   Number of Beta  Electrons                 15.9999969854 
   Total number of  Electrons                31.9999939709 
   Exchange energy                          -21.4478559475 
   Correlation energy                        -1.2739186311 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7217745787 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1741545163 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8395     6.0000     0.1605     4.2156     4.2156    -0.0000
  1   0     8.3762     8.0000    -0.3762     2.1096     2.1096    -0.0000
  2   0     7.1934     7.0000    -0.1934     3.0359     3.0359    -0.0000
  3   0     6.1756     6.0000    -0.1756     3.8268     3.8268     0.0000
  4   0     0.9138     1.0000     0.0862     0.9996     0.9996    -0.0000
  5   0     0.8460     1.0000     0.1540     1.0159     1.0159     0.0000
  6   0     0.8675     1.0000     0.1325     1.0190     1.0190     0.0000
  7   0     0.8941     1.0000     0.1059     0.9866     0.9866     0.0000
  8   0     0.8940     1.0000     0.1060     0.9865     0.9865    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                1.986699
                0             6               2            7                1.208886
                0             6               4            1                0.984751
                2             7               3            6                0.877981
                2             7               5            1                0.963400
                3             6               6            1                0.965014
                3             6               7            1                0.977028
                3             6               8            1                0.976950
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0084998663
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -209.1776524131
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 15.9999974357 
   Number of Beta  Electrons                 15.9999974357 
   Total number of  Electrons                31.9999948714 
   Exchange energy                          -21.4709728182 
   Correlation energy                        -1.2751545164 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7461273346 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1776524131 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0085150028
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -209.1784496845
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 15.9999984985 
   Number of Beta  Electrons                 15.9999984985 
   Total number of  Electrons                31.9999969970 
   Exchange energy                          -21.4754865164 
   Correlation energy                        -1.2754036897 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7508902061 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1784496845 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0085183195
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -209.1785706991
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 15.9999988227 
   Number of Beta  Electrons                 15.9999988227 
   Total number of  Electrons                31.9999976455 
   Exchange energy                          -21.4731842269 
   Correlation energy                        -1.2752660756 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7484503026 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1785706991 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0085158236
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -209.1785888860
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 15.9999986148 
   Number of Beta  Electrons                 15.9999986148 
   Total number of  Electrons                31.9999972296 
   Exchange energy                          -21.4721791323 
   Correlation energy                        -1.2752021957 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7473813280 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1785888860 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0085146878
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:     -209.1785922198
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 15.9999985247 
   Number of Beta  Electrons                 15.9999985247 
   Total number of  Electrons                31.9999970495 
   Exchange energy                          -21.4718125641 
   Correlation energy                        -1.2751774647 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7469900287 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1785922198 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0085143139
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:     -209.1785944205
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 15.9999983711 
   Number of Beta  Electrons                 15.9999983711 
   Total number of  Electrons                31.9999967422 
   Exchange energy                          -21.4720953493 
   Correlation energy                        -1.2751942851 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7472896344 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1785944205 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0085145810
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 8
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 8
   prop. index: 1
        SCF Energy:     -209.1785944283
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 8
   prop. index: 1
   Number of Alpha Electrons                 15.9999983485 
   Number of Beta  Electrons                 15.9999983485 
   Total number of  Electrons                31.9999966971 
   Exchange energy                          -21.4721993589 
   Correlation energy                        -1.2752011281 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -22.7474004870 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -209.1785944283 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 8
   prop. index: 1
     Number of atoms                     : 9
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 8
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.8307     6.0000     0.1693     4.2236     4.2236    -0.0000
  1   0     8.3565     8.0000    -0.3565     2.1411     2.1411     0.0000
  2   0     7.1903     7.0000    -0.1903     3.0459     3.0459    -0.0000
  3   0     6.1804     6.0000    -0.1804     3.8215     3.8215    -0.0000
  4   0     0.9409     1.0000     0.0591     1.0017     1.0017    -0.0000
  5   0     0.8514     1.0000     0.1486     1.0200     1.0200    -0.0000
  6   0     0.8719     1.0000     0.1281     1.0226     1.0226    -0.0000
  7   0     0.8890     1.0000     0.1110     0.9862     0.9862    -0.0000
  8   0     0.8889     1.0000     0.1111     0.9864     0.9864    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            8                2.007307
                0             6               2            7                1.203030
                0             6               4            1                0.981704
                2             7               3            6                0.877785
                2             7               5            1                0.972392
                3             6               6            1                0.967380
                3             6               7            1                0.976322
                3             6               8            1                0.976402
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 8
   prop. index: 1
        Van der Waals Correction:       -0.0085146376
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 8
   prop. index: 1
       Filename                          : opt.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        3.9628893279
        Electronic Contribution:
                  0    
      0      -3.221342
      1      -0.295245
      2      -0.755737
        Nuclear Contribution:
                  0    
      0       4.570493
      1      -0.010471
      2       0.036649
        Total Dipole moment:
                  0    
      0       1.349151
      1      -0.305716
      2      -0.719088
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     1 
    Coordinates:
               0 C      1.143530000000   -0.030680000000   -0.003050000000
               1 O      0.444450000000    0.348870000000    0.924270000000
               2 N      2.499160000000    0.000600000000    0.073330000000
               3 C      3.225490000000    0.475580000000    1.233730000000
               4 H      0.667400000000   -0.400160000000   -0.905730000000
               5 H      3.043630000000   -0.330940000000   -0.736690000000
               6 H      2.548710000000    0.809100000000    2.048550000000
               7 H      3.868670000000    1.331020000000    0.939420000000
               8 H      3.868670000000   -0.340710000000    1.623700000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     2 
    Coordinates:
               0 C      1.142354357699   -0.022017217053    0.018083394086
               1 O      0.417301031867    0.350009795756    0.926144554209
               2 N      2.496299398590    0.002449367073    0.076928868489
               3 C      3.238410034982    0.473149656988    1.227717449635
               4 H      0.703211963635   -0.403601232590   -0.912658917043
               5 H      3.015314098840   -0.323556740504   -0.718887714891
               6 H      2.554537734367    0.791955367002    2.006998587000
               7 H      3.871239721344    1.319789181987    0.950124283246
               8 H      3.871041658676   -0.325498178659    1.623079495270
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     3 
    Coordinates:
               0 C      1.139450358127   -0.014709745313    0.035891042962
               1 O      0.401049031354    0.353848259145    0.929102979279
               2 N      2.491462026546    0.003200306775    0.078183161693
               3 C      3.246917674613    0.471442731480    1.223808743837
               4 H      0.736424444032   -0.405771587533   -0.914063573701
               5 H      2.995423036722   -0.325888830700   -0.723843536314
               6 H      2.546101807696    0.782184248969    1.987541514893
               7 H      3.875674205771    1.319095474772    0.956044655496
               8 H      3.877207415139   -0.320720857594    1.624865011856
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     4 
    Coordinates:
               0 C      1.136651247321   -0.011466240230    0.040897404818
               1 O      0.397020970931    0.356149072759    0.930247844811
               2 N      2.489473799908    0.003584664439    0.078553457517
               3 C      3.249784694175    0.471127071659    1.223211623043
               4 H      0.750077126021   -0.407857937051   -0.915172563267
               5 H      2.986266291346   -0.328861066913   -0.729044299624
               6 H      2.541130160323    0.779685104262    1.985730829406
               7 H      3.878008699033    1.321893567208    0.957387644384
               8 H      3.881297010942   -0.321574236133    1.625718058913
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     5 
    Coordinates:
               0 C      1.135205802331   -0.012286270031    0.041503164987
               1 O      0.397369118577    0.358935499408    0.929795762855
               2 N      2.489415544168    0.002904475568    0.079402915571
               3 C      3.250097824396    0.471199823689    1.223691716761
               4 H      0.753462044990   -0.407742374173   -0.916634444455
               5 H      2.983162169179   -0.330929732825   -0.730397454843
               6 H      2.540792808432    0.778076127897    1.987732432174
               7 H      3.876612712377    1.324012948878    0.957441289666
               8 H      3.883591975549   -0.321490498411    1.624994617284
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     6 
    Coordinates:
               0 C      1.134261348895   -0.009305462797    0.039724675815
               1 O      0.398200111286    0.359224243676    0.930120020265
               2 N      2.489741041956    0.003728508894    0.079705358954
               3 C      3.249981220185    0.471362907178    1.224154506633
               4 H      0.755191418082   -0.410247571462   -0.916780646825
               5 H      2.981086569693   -0.332497055144   -0.730839141092
               6 H      2.541357563249    0.777441641929    1.989427041251
               7 H      3.875925508991    1.324939719199    0.958009168007
               8 H      3.883965217662   -0.321966931474    1.624009016993
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     7 
    Coordinates:
               0 C      1.134420712943   -0.011123903488    0.040176469596
               1 O      0.398317813982    0.361356311047    0.929114739086
               2 N      2.489912734917    0.002661736534    0.080272299017
               3 C      3.249823954267    0.471264692022    1.224204591852
               4 H      0.754739609087   -0.408826600400   -0.917381218601
               5 H      2.981238495060   -0.333373650345   -0.730083594299
               6 H      2.541844825155    0.775889017022    1.990137619710
               7 H      3.874046766337    1.325806530391    0.957942543327
               8 H      3.885365088253   -0.320974132784    1.623146550312
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    9 
    Geometry Index:     8 
    Coordinates:
               0 C      1.134570348317   -0.010770749633    0.040048068963
               1 O      0.398184176344    0.362219255998    0.928644933848
               2 N      2.489928064176    0.002378876801    0.080403055866
               3 C      3.249811062310    0.471181644254    1.224158193586
               4 H      0.754590163704   -0.408861237495   -0.917279625040
               5 H      2.981280520439   -0.334134245321   -0.729657107788
               6 H      2.541952528046    0.774840715550    1.990436890312
               7 H      3.873187089015    1.326373291616    0.958227019753
               8 H      3.886206047650   -0.320547551769    1.622548570501
