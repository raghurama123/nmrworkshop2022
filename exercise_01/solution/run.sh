export OMP_NUM_THREADS=1

obabel -ismi structures.smi -oxyz --gen3d > structures.xyz 

obabel -osvg structures.smi > structures.svg
