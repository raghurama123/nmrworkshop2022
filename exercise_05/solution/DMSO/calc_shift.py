import numpy as np

# Isotropic shielding of TMS
tms_C=184.1
tms_H=31.7

# Isotropic shielding of cis-NMF
H = [ 23.409, 26.538, 28.353, 28.826, 28.824 ]
C = [ 14.382, 155.056]
Hexp=[8.0, 7.4, 2.9, 2.9, 2.9]
Cexp=[166.5,28.2]

Hdelta=[]
for iH in H:
  print("%5.2f" % (tms_H-iH))
  Hdelta.append(tms_H-iH)

# calculate Mean error
mae=np.mean(np.abs(np.array(Hdelta)-np.array(Hexp)))
print("MAE = %5.2f ppm" % (mae))

Cdelta=[]
for iC in C:
  print("%5.2f" % (tms_C-iC))
  Cdelta.append(tms_C-iC)

# calculate Mean error
mae=np.mean(np.abs(np.array(Cdelta)-np.array(Cexp)))
print("MAE = %5.2f ppm" % (mae))

Hexp=[8.2, 7.4, 2.8, 2.8, 2.8]
Cexp=[163.3,24.8]

# Isotropic shielding of trans-NMF
H = [ 23.217, 26.105, 27.665, 29.284, 29.278]
C = [ 18.001, 160.301]

Hdelta=[]
for iH in H:
  print("%5.2f" % (tms_H-iH))
  Hdelta.append(tms_H-iH)

# calculate Mean error
mae=np.mean(np.abs(np.array(Hdelta)-np.array(Hexp)))
print("MAE = %5.2f ppm" % (mae))

Cdelta=[]
for iC in C:
  print("%5.2f" % (tms_C-iC))
  Cdelta.append(tms_C-iC)

# calculate Mean error
mae=np.mean(np.abs(np.array(Cdelta)-np.array(Cexp)))
print("MAE = %5.2f ppm" % (mae))
