export PATH=/home/Lib/openmpi-4.1.1-install/bin:$PATH
export LD_LIBRARY_PATH=/home/Lib/openmpi-4.1.1-install/lib:$LD_LIBRARY_PATH

export PATH=/home/Lib/ORCA_503:$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/Lib/ORCA_503


cd cis_NMF/nmr
/home/Lib/ORCA_503/orca nmr.com > nmr.out
cd ../../

cd trans_NMF/nmr
/home/Lib/ORCA_503/orca nmr.com > nmr.out
cd ../../

cd tms/nmr
/home/Lib/ORCA_503/orca nmr.com > nmr.out
cd ../../
